import 'dart:async';
import 'dart:math';
import 'package:google_fonts/google_fonts.dart';

import '../dashboard.dart';

import '../others/globalVariables.dart';
import '../others/globalFormulas.dart';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../welcome.dart';

  

class BootAnimation extends StatefulWidget
{
  @override
  _BootAnimationState createState() => _BootAnimationState();
}

class _BootAnimationState extends State<BootAnimation>
{
  int randomNumber = Random().nextInt(1722);
  int randomNumber2 = Random().nextInt(300);



  Future iniciar() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userNome = (prefs.getString('nome') ?? "");
    userSenha = (prefs.getString('password') ?? "");
    isLogged = (prefs.getInt('isLogged') ?? 0);

    if(isLogged == 1)
    {
      Navigator.pushReplacement(context,MaterialPageRoute
      (
        builder: (context)=>Dashboard(), 
      ));
    }
    else
    {
      Navigator.pushReplacement(context,MaterialPageRoute
      (
        builder: (context)=>Login(), 
      ));
    }
  }

  @override
  void initState()
  {
    super.initState();
    
    Timer(Duration(milliseconds:80 + randomNumber2+randomNumber),()
    {
      welcomeMensage();
      iniciar();
      
    });
  }
    


//--- Interface
  @override
  Widget build(BuildContext context)
  {
    return Scaffold
    (
      body: Container
      (
        child: Column
        (
          children: <Widget>
          [
            Expanded
            (
              child: Container //Kcalculator logo com loading
              (
                child: Column
                (
                  children: <Widget>
                  [
                    Container //Kcalculator
                    (
                      child: RichText
                      (
                        text: TextSpan
                        (
                          children: <TextSpan>
                          [
                            TextSpan
                            (
                              text: "HEA",
                              style: GoogleFonts.roboto
                              (
                                fontSize: 50,
                                color: color_light,
                                fontWeight: FontWeight.w900,
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                            
                             TextSpan
                            (
                              text: "PPY",
                              style: GoogleFonts.roboto
                              (
                                fontSize: 50,
                                color: color_orange,
                                fontWeight: FontWeight.w900,
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                          ],
                        ),
                      ),
                      height: 60,
                    ),

                    Container //Loading
                    (
                      child: SizedBox // loading
                      (
                        child: CircularProgressIndicator
                        (
                          valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                        ),
                        height: 15.0,
                        width: 15.0,
                      ),
                      padding: EdgeInsets.only(top: 20),
                    ),
                    
                  ],

                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                ),

                width: double.infinity,
              ),
              flex: 9,
            ),


            Expanded
            (
              child:Container
              (
                child: ClipRRect
                (
                  borderRadius: BorderRadius.circular(50.0),
                  child: Image.asset
                  (
                    'assets/images/logo4.png',
                    height: 30,
                    //fit: BoxFit.fill,
                  ),
                ),
                margin: const EdgeInsets.only(bottom: 10,),
                alignment: Alignment.bottomCenter,
              ),
              flex: 1,
            ),
          ],
        ),
      ),
      backgroundColor: Color.fromRGBO(0, 0, 0, 1),
    );
  }
}
