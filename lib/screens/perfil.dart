import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:heappy/others/globalFormulas.dart';
import 'package:heappy/others/globalVariables.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:url_launcher/url_launcher.dart';
import '../welcome.dart';
//===============================declaracao dos sexos===============================

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  String? _valorAtividade;
  String? _valorObjetivo;

  


  TextEditingController massaMain = TextEditingController();
  TextEditingController alturaMain = TextEditingController();
  TextEditingController idadeMain = TextEditingController();
  TextEditingController abdomenMain = TextEditingController();
  TextEditingController pescocoMain = TextEditingController();
  TextEditingController quadrilMain = TextEditingController();


  void passValor()
  {
    dropButton_atividade_perfil_g = _valorAtividade.toString();
    dropButton_objetivo_perfil_g = _valorObjetivo.toString();

    massa_perfil_g = double.parse(massaMain.text.toString());
    altura_perfil_g = double.parse(alturaMain.text.toString());
    idade_perfil_g = double.parse(idadeMain.text.toString());
    pescoco_perfil_g = double.parse(pescocoMain.text.toString());
    abdomem_perfil_g = double.parse(abdomenMain.text.toString());

    if(sexo_perfil_g == 1)
    { 
      quadril_perfil_g = double.parse(quadrilMain.text.toString());
    }
  }


  Future save() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt("sexo_perfil_g", sexo_perfil_g);
    prefs.setDouble("massa_perfil_g", massaMain.text != "" ? double.parse(massaMain.text) : 0);
    prefs.setDouble("altura_perfil_g", alturaMain.text != "" ? double.parse(alturaMain.text) : 0);
    prefs.setDouble("idade_perfil_g", idadeMain.text != "" ? double.parse(idadeMain.text) : 0);
    prefs.setDouble("abdomem_perfil_g", abdomenMain.text != "" ? double.parse(abdomenMain.text) : 0);
    prefs.setDouble("pescoco_perfil_g", pescocoMain.text != "" ? double.parse(pescocoMain.text) : 0);
    prefs.setDouble("quadril_perfil_g", quadrilMain.text != "" ? double.parse(quadrilMain.text) : 0);
    prefs.setString("dropButton_atividade_perfil_g", _valorAtividade.toString());
    prefs.setString("dropButton_objetivo_perfil_g", _valorObjetivo.toString());

    prefs.setDouble('imc_perfil_g', imc_perfil_g);
    prefs.setDouble('tmb_perfil_g', tmb_perfil_g);
    prefs.setDouble('lbm_perfil_g', lbm_perfil_g);
    prefs.setDouble('gc_perfil_g', gc_perfil_g);
    prefs.setDouble('pmm_perfil_g', pmm_perfil_g);
    prefs.setDouble('pmm_perfil_10_g', pmm_perfil_10_g);
    prefs.setDouble('pmm_perfil_15_g', pmm_perfil_15_g);
    prefs.setDouble('min_pi_perfil_g', min_pi_perfil_g);
    prefs.setDouble('max_pi_perfil_g', max_pi_perfil_g);
    prefs.setDouble('tdee_perfil_g', tdee_perfil_g);
  }


  
  




  Future<void> sobreIntensidade() {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Nível de Atividade',
            
            style: GoogleFonts.montserrat
            (
              fontSize: 19,
              color: color_light,
              fontWeight: FontWeight.w700,
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  'Sedentário',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 15,
                    color: color_light,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  'Passa a maior parte do dia sentado ou em repouso. Ex: professores, secretária...',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  '',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  'Exercício Leve',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 15,
                    color: color_light,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  'Passa a maior parte do dia em pé ou praticam exercícios (caminhadas, pilates...) 1x ou 2x por semana. Ex: cozinheiros, faxineiras..',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  '',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  'Exercício Moderado',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 15,
                    color: color_light,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  'Tem um dia muito ativo e aindam realizam atividades físicas (treinamentos com pesos, corridas, lutas...) de 3x a 5x por semana. Ex: Coletor de lixo, pedreiros...',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  '',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  'Exercício Extremo',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  'Tem um dia muito ativo e aindam realizam atividades físicas muito intensas (treinamentos com pesos, corridas, lutas...) de 6x a 7x por semana. Ex: Coletor de lixo, pedreiros...',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  '',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  'Atleta Profissional',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 15,
                    color: color_light,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  'Atleta profissionais de altíssimo nível. Ex: Atletas de futebol, maratonistas...',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  '',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton
            (
              child: Text
              (
                "Fechar",
                style: GoogleFonts.roboto
                (
                  fontSize: 15,
                  color: color_orange,
                  fontWeight: FontWeight.w700,
                  //fontWeight: FontWeight.w800,
                  //fontStyle: FontStyle.italic,
                ),
              ),
              onPressed: ()
              {
                Navigator.of(context).pop();
              },
            ),
          ],
          backgroundColor: color_dark
        );
      },
    );
  }

  Future<void> sobreObjetivo() {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Nível de Atividade',
            style: GoogleFonts.montserrat
            (
              fontSize: 19,
              color: color_light,
              fontWeight: FontWeight.w700,
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  'Perda de Peso',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 15,
                    color: color_light,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  'Tem como objetivo a perca de peso gradual (perca aproximada de 0.3kg por semana).',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  '',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  'Manter o Peso',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 15,
                    color: color_light,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  'Tem como objetivo a manutenção do peso, ou seja, apenas é calculado do TDEE.',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  '',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  'Ganho de Peso',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 15,
                    color: color_light,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  'Tem como objetivo o ganho de peso gradual (ganho aproximado de 0.3kg por semana)',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  '',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  '',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  '',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
                Text(
                  'Foram adotados valores conservadores para a o ganho e perca de peso, por vários quesitos, principalmente saúde geral, qualidade corpórea e qualidade de vida.',
                  style: GoogleFonts.roboto
                  (
                    fontSize: 14,
                    color: color_light,
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton
            (
              child: Text
              (
                "Fechar",
                style: GoogleFonts.roboto
                (
                  fontSize: 15,
                  color: color_orange,
                  fontWeight: FontWeight.w700,
                  //fontWeight: FontWeight.w800,
                  //fontStyle: FontStyle.italic,
                ),
              ),
              onPressed: ()
              {
                Navigator.of(context).pop();
              },
            ),
          ],
          backgroundColor: color_dark
        );
      },
    );
  }



  @override
  void initState()
  {
    super.initState();
    setState(() {
      sexo_perfil_g = 1;
    });
  }

  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            Container //Container pra esconder a barra de status
                (
              height: MediaQuery.of(context).padding.top,
              color: color_dark,
            ),
            Expanded(
              child: Container(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        child: RichText(
                          text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                text: "${horaMensagem}, ",
                                style: GoogleFonts.roboto(
                                  fontSize: 27,
                                  color: color_light,
                                  fontWeight: FontWeight.w900,
                                  fontStyle: FontStyle.italic,
                                ),
                              ),
                              TextSpan(
                                text: "${userNome}! ",
                                style: GoogleFonts.roboto(
                                  fontSize: 27,
                                  color: color_orange,
                                  fontWeight: FontWeight.w900,
                                  fontStyle: FontStyle.italic,
                                ),
                              ),
                            ],
                          ),
                          textAlign: TextAlign.left,
                        ),
                        //height: 60,
                        alignment: Alignment.centerLeft,
                      ),
                      flex: 9,
                    ),
                    Expanded(
                        child: Align(
                          alignment: Alignment.center,
                          child: Container(
                            child: Container(
                              child: IconButton(
                                icon: Icon(
                                  Icons.group,
                                  color: color_light,
                                ),
                                onPressed: () {
                                  //criar um AlertDialog pra mostrar o nome do usuário
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: Text(
                                          "Grupo Stack Underflow ",
                                          style: GoogleFonts.ptSans(
                                            fontSize: 20,
                                            color: color_light,
                                            fontWeight: FontWeight.w600,
                                            //fontStyle: FontStyle.italic,
                                          ),
                                        ),
                                        content: SingleChildScrollView(
                                          child: ListBody(
                                            children: <Widget>[
                                              Text(
                                                '- DANIEL BORBA GUIMARÃES DA COSTA',
                                                style: GoogleFonts.roboto(
                                                  fontSize: 14,
                                                  color: color_light,
                                                ),
                                              ),
                                              Text(
                                                '- DAVID GABRIEL BEILFUSS JORGE',
                                                style: GoogleFonts.roboto(
                                                  fontSize: 14,
                                                  color: color_light,
                                                ),
                                              ),
                                              Text(
                                                '- MURILLO SAMPAIO OLIVEIRA ANTONIO',
                                                style: GoogleFonts.roboto(
                                                  fontSize: 14,
                                                  color: color_light,
                                                ),
                                              ),
                                              Text(
                                                '- OTÁVIO AUGUSTO RIBEIRO ARAÚJO',
                                                style: GoogleFonts.roboto(
                                                  fontSize: 14,
                                                  color: color_light,
                                                ),
                                              ),
                                              Text(
                                                '- SÔNIA RESENDE DE ARAÚJO',
                                                style: GoogleFonts.roboto(
                                                  fontSize: 14,
                                                  color: color_light,
                                                ),
                                              ),
                                              Text(
                                                '- THAÍS RESENDE ARAÚJO BORGES BONFIM',
                                                style: GoogleFonts.roboto(
                                                  fontSize: 14,
                                                  color: color_light,
                                                ),
                                              ),
                                              Text(
                                                '',
                                                style: TextStyle(
                                                    fontFamily: 'ProductSans',
                                                    fontSize: 15),
                                              ),
                                              

                                              GestureDetector
                                              (
                                                onTap: () => launch('https://gitlab.com/otavio123ribeiro/heappy'),
                                                child: Card
                                                (
                                                  child: Row
                                                  (
                                                    children: <Widget>
                                                    [
                                                      Expanded
                                                      (
                                                        flex: 3, // 60%
                                                        child: IconButton
                                                        (
                                                          icon: Image.asset("assets/icon/interface/gitlab.png"),
                                                          onPressed: () {},
                                                        ),
                                                      ),
                                                      
                                                      Expanded
                                                      (
                                                        flex: 21, // 60%
                                                        child: Text
                                                        (
                                                          "GitLab Link",
                                                          style: GoogleFonts.roboto
                                                          (
                                                            fontSize: 14,
                                                            color: color_black,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  shape: RoundedRectangleBorder
                                                  (
                                                    borderRadius: BorderRadius.circular(30.0),
                                                  ),
                                                  color: color_light,
                                                ),
                                              ),
                                              
                                              
                                              Text(
                                                '',
                                                style: TextStyle(
                                                    fontFamily: 'ProductSans',
                                                    fontSize: 15),
                                              ),
                                              Text(
                                                'A ideia principal do aplicativo é realizar cálculos através de valores informados pelo usuário. Alguns deles são: porcentagem de gordura corporal, índice de massa corporal, taxa metabólica basal, gasto calórico diário, peso ideal, potencial muscular máximo...  Até o momento está previsto ao menos 4 "telas", onde 1 será uma provável tela de login, 1 será para o usuário inserir/atualizar suas informações, 1 será para mostrar os resultados calculados. Outras "telas" também podem ser inseridas futuramente. Os valores necessários para realizar os cálculos serão por exemplo: altura, idade, peso, sexo... Com essas informações, será possível realizar cálculos através de funções.Se faz necessário ressaltar que a aplicação terá validação médica, sendo a palavra de um responsável como final para quaisquer fins. No entanto, serão utilizados fórmulas e métodos muito conhecidos como as fórmulas de Mifflin-St Jeor, a equação revisada de Harris-Benedict. Ainda não estão definidos quais serão as bibliotecas, ou pacotes externos serão utilizados, no entanto, provavelmente serão necessárias soluções para salvamento de informações de forma local, exibição de gráficos (se for decidido sua implementação),  uma forma simples de mostrar avisos (possivelmente através de "toast notifications"). Já está sendo analisada a possibilidade de uso de um banco de dados, mas nada definido, tendo em vista que o prazo para desenvolvimento é curto e outros fatores.',
                                                style: GoogleFonts.roboto
                                                (
                                                  fontSize: 14,
                                                  color: color_light,
                                                ),
                                              ),
                                              Text(
                                                '',
                                                style: TextStyle(
                                                    fontFamily: 'ProductSans',
                                                    fontSize: 15),
                                              ),
                                              Text(
                                                'É importante para nós que os usuários permaneçam saudáveis enquanto atingem suas metas de condicionamento físico e bem-estar. Seja responsável e use seu melhor julgamento e bom senso. Fornecemos nossos serviços apenas para fins informativos e não podemos ser responsabilizados em caso de lesão ou se caso apresentar um problema de saúde. Em particular, embora a maior parte do conteúdo publicado pelos outros usuários em nossa comunidade seja útil, ela é proveniente de estranhos na Internet e nunca deve prevalecer sobre o bom senso ou o aconselhamento médico real. Ao usar nossos Serviços, você concorda, representa e garante que recebeu o consentimento do seu médico para participar dos Programas ou de qualquer uma das atividades relacionadas disponibilizadas para você em conexão com os Serviços. Além disso, você concorda, declara e garante que consultou seu médico antes de fazer qualquer alteração alimentar com base nas informações disponíveis através dos Serviços. As condições e habilidades de todos são diferentes, e participar dos Programas e outras atividades promovidas por nossos Serviços é por sua conta e risco. Se você optar por participar desses Programas e outras atividades, você o faz por sua livre e espontânea vontade, concordando, consciente e voluntariamente, assumindo todos os riscos associados a tais atividades. Os programas e outras atividades promovidas pelos Serviços podem representar riscos mesmo para aqueles que estão atualmente com boa saúde.',
                                                style: GoogleFonts.roboto
                                                (
                                                  fontSize: 14,
                                                  color: color_light,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: Text(
                                              "Fechar",
                                              style: GoogleFonts.roboto
                                              (
                                                fontSize: 15,
                                                color: color_orange,
                                                fontWeight: FontWeight.w800,
                                                fontStyle: FontStyle.italic,
                                              ),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                        backgroundColor: color_dark,
                                      );
                                    },
                                  );
                                },
                              ),
                              decoration: BoxDecoration(
                                  color: color_dark,
                                  border:
                                      Border.all(color: color_orange, width: 1),
                                  //border: Border.all(color: color_orange, width: 5.0),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(35.0)),
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/wavey-fingerprint.png'),
                                      fit: BoxFit.cover)),
                              margin: EdgeInsets.only(right: 20),
                              height: 38,
                              //width: 43,
                              alignment: Alignment.center,
                            ),
                          ),
                        ),
                        flex: 2),
                  ],
                ),
                decoration: BoxDecoration(
                  color: color_dark,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(35),
                    bottomRight: Radius.circular(35),
                  ),
                ),
                width: double.infinity,
                padding: EdgeInsets.only(left: 20),
              ),
              flex: 2,
            ),
            Expanded(
                child: Container(
                  child: SingleChildScrollView(
                    child: Container(
                      child: Container(
                        child: Column(
                          children: <Widget>[
//================================================Container do sexo================================================

                            Container(
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: ListTile(
                                      title: Text('Feminino',
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Colors.white)),
                                      leading: Theme(
                                        data: ThemeData(
                                            unselectedWidgetColor: color_white),
                                        child: Radio(
                                          value: 1,
                                          groupValue: sexo_perfil_g,
                                          onChanged: (newValue) {
                                            setState(() {
                                              sexo_perfil_g = int.parse(
                                                  newValue.toString());
                                              print(sexo_perfil_g);
                                            });
                                          },
                                          activeColor: color_orange,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: ListTile(
                                      title: Text('Maculino',
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Colors.white)),
                                      leading: Theme(
                                        data: ThemeData(
                                            unselectedWidgetColor: color_white),
                                        child: Radio(
                                          value: 2,
                                          groupValue: sexo_perfil_g,
                                          onChanged: (newValue) {
                                            setState(() {
                                              sexo_perfil_g = int.parse(
                                                  newValue.toString());
                                              print(sexo_perfil_g);
                                            });
                                          },
                                          activeColor: color_orange,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              margin: EdgeInsets.only(bottom: 5),
                              height: 35,
                            ),

                            /*
================================================Container de peso================================================
 */
                            Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 8, vertical: 16),
                              child: new Theme(
                                data: new ThemeData(
                                  primaryColor: Colors.redAccent,
                                  primaryColorDark: Colors.red,
                                ),
                                child: new TextField(
                                  decoration: new InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(30)),
                                      borderSide: BorderSide(
                                          width: 2, color: color_white),
                                    ),

                                    enabledBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(30)),
                                      borderSide: BorderSide(
                                          width: 2, color: color_white),
                                    ),

                                    errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30)),
                                        borderSide: BorderSide(
                                            width: 1, color: Colors.black)),

                                    hintText: 'Ex: 75.2',
                                    hintStyle: TextStyle(
                                        fontSize: 20,
                                        color:
                                            Color.fromRGBO(255, 255, 255, 0.5)),

                                    //helperText: '',

                                    labelText: 'Peso',

                                    labelStyle: TextStyle(
                                      color: color_white,
                                    ),

                                    prefixIcon: Icon(
                                      Icons.line_weight_rounded,
                                      color: color_orange,
                                    ),

                                    prefixText: ' ',

                                    suffixText: 'Kg',

                                    suffixStyle: TextStyle(color: color_orange),
                                  ),
                                  controller: massaMain,
                                  keyboardType: TextInputType.number,
                                  style: TextStyle(color: color_white),
                                ),
                              ),
                            ),
                            /*
================================================Container de altura================================================
 */
                            Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 8, vertical: 16),
                              child: new Theme(
                                data: new ThemeData(
                                  primaryColor: Colors.redAccent,
                                  primaryColorDark: Colors.red,
                                ),
                                child: new TextField(
                                  decoration: new InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(30)),
                                      borderSide: BorderSide(
                                          width: 2, color: color_white),
                                    ),

                                    enabledBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(30)),
                                      borderSide: BorderSide(
                                          width: 2, color: color_white),
                                    ),

                                    errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30)),
                                        borderSide: BorderSide(
                                            width: 1, color: Colors.black)),

                                    hintText: 'Ex: 186',
                                    hintStyle: TextStyle(
                                        fontSize: 20,
                                        color:
                                            Color.fromRGBO(255, 255, 255, 0.5)),

                                    //helperText: '',

                                    labelText: 'Altura',

                                    labelStyle: TextStyle(
                                      color: color_white,
                                    ),

                                    prefixIcon: Icon(
                                      Icons.line_weight_rounded,
                                      color: color_orange,
                                    ),

                                    prefixText: ' ',

                                    suffixText: 'cm',

                                    suffixStyle: TextStyle(color: color_orange),
                                  ),
                                  controller: alturaMain,
                                  keyboardType: TextInputType.number,
                                  style: TextStyle(color: color_white),
                                ),
                              ),
                            ),

/*
================================================Container de idade================================================
 */

                            Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 8, vertical: 16),
                              child: new Theme(
                                data: new ThemeData(
                                  primaryColor: Colors.redAccent,
                                  primaryColorDark: Colors.red,
                                ),
                                child: new TextField(
                                  decoration: new InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(30)),
                                      borderSide: BorderSide(
                                          width: 2, color: color_white),
                                    ),

                                    enabledBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(30)),
                                      borderSide: BorderSide(
                                          width: 2, color: color_white),
                                    ),

                                    errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30)),
                                        borderSide: BorderSide(
                                            width: 1, color: Colors.black)),

                                    hintText: 'Ex: 18',
                                    hintStyle: TextStyle(
                                        fontSize: 20,
                                        color:
                                            Color.fromRGBO(255, 255, 255, 0.5)),

                                    //helperText: '',

                                    labelText: 'Idade',

                                    labelStyle: TextStyle(
                                      color: color_white,
                                    ),

                                    prefixIcon: Icon(
                                      Icons.line_weight_rounded,
                                      color: color_orange,
                                    ),

                                    prefixText: ' ',

                                    suffixText: 'anos',

                                    suffixStyle: TextStyle(color: color_orange),
                                  ),
                                  controller: idadeMain,
                                  keyboardType: TextInputType.number,
                                  style: TextStyle(color: color_white),
                                ),
                              ),
                            ),

/*
================================================Container de abdomen================================================
 */
                            Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 8, vertical: 16),
                              child: new Theme(
                                data: new ThemeData(
                                  primaryColor: Colors.redAccent,
                                  primaryColorDark: Colors.red,
                                ),
                                child: new TextField(
                                  decoration: new InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(30)),
                                      borderSide: BorderSide(
                                          width: 2, color: color_white),
                                    ),

                                    enabledBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(30)),
                                      borderSide: BorderSide(
                                          width: 2, color: color_white),
                                    ),

                                    errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30)),
                                        borderSide: BorderSide(
                                            width: 1, color: Colors.black)),

                                    hintText: 'Ex: 80',
                                    hintStyle: TextStyle(
                                        fontSize: 20,
                                        color:
                                            Color.fromRGBO(255, 255, 255, 0.5)),

                                    //helperText: '',

                                    labelText: 'Circunferência do Abdômen',

                                    labelStyle: TextStyle(
                                      color: color_white,
                                    ),

                                    prefixIcon: Icon(
                                      Icons.line_weight_rounded,
                                      color: color_orange,
                                    ),

                                    prefixText: ' ',

                                    suffixText: 'cm',

                                    suffixStyle: TextStyle(color: color_orange),
                                  ),
                                  controller: abdomenMain,
                                  keyboardType: TextInputType.number,
                                  style: TextStyle(color: color_white),
                                ),
                              ),
                            ),

/*
================================================Container de pescoço================================================
 */
                            Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 8, vertical: 16),
                              child: new Theme(
                                data: new ThemeData(
                                  primaryColor: Colors.redAccent,
                                  primaryColorDark: Colors.red,
                                ),
                                child: new TextField(
                                  decoration: new InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(30)),
                                      borderSide: BorderSide(
                                          width: 2, color: color_white),
                                    ),

                                    enabledBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(30)),
                                      borderSide: BorderSide(
                                          width: 2, color: color_white),
                                    ),

                                    errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30)),
                                        borderSide: BorderSide(
                                            width: 1, color: Colors.black)),

                                    hintText: 'Ex: 35',
                                    hintStyle: TextStyle(
                                        fontSize: 20,
                                        color:
                                            Color.fromRGBO(255, 255, 255, 0.5)),

                                    //helperText: '',

                                    labelText: 'Circunferência do Pescoço',

                                    labelStyle: TextStyle(
                                      color: color_white,
                                    ),

                                    prefixIcon: Icon(
                                      Icons.line_weight_rounded,
                                      color: color_orange,
                                    ),

                                    prefixText: ' ',

                                    suffixText: 'cm',

                                    suffixStyle: TextStyle(color: color_orange),
                                  ),
                                  controller: pescocoMain,
                                  keyboardType: TextInputType.number,
                                  style: TextStyle(color: color_white),
                                ),
                              ),
                            ),

                            /*
================================================Container do Quadril================================================
 */
                            sexo_perfil_g == 2
                                ? Container(color: Colors.transparent)
                                : Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 8, vertical: 16),
                                    child: new Theme(
                                      data: new ThemeData(
                                        primaryColor: Colors.redAccent,
                                        primaryColorDark: Colors.red,
                                      ),
                                      child: new TextField(
                                        decoration: new InputDecoration(
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(30)),
                                            borderSide: BorderSide(
                                                width: 2, color: color_white),
                                          ),

                                          enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(30)),
                                            borderSide: BorderSide(
                                                width: 2, color: color_white),
                                          ),

                                          errorBorder: OutlineInputBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(30)),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.black)),

                                          hintText: 'Ex: 96',
                                          hintStyle: TextStyle(
                                              fontSize: 20,
                                              color: Color.fromRGBO(
                                                  255, 255, 255, 0.5)),

                                          //helperText: '',

                                          labelText: 'Circunferência do Quadril',

                                          labelStyle: TextStyle(
                                            color: color_white,
                                          ),

                                          prefixIcon: Icon(
                                            Icons.line_weight_rounded,
                                            color: color_orange,
                                          ),

                                          prefixText: ' ',

                                          suffixText: 'cm',

                                          suffixStyle:
                                              TextStyle(color: color_orange),
                                        ),
                                        controller: quadrilMain,
                                        keyboardType: TextInputType.number,
                                        style: TextStyle(color: color_white),
                                      ),
                                    ),
                                  ),
                            /*
================================================Container de dropdown================================================
 */
                            Container(
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                            child: Theme(
                                                data: ThemeData(
                                                    canvasColor: color_dark),
                                                child: DropdownButton<String>(
                                                    isExpanded: true,
                                                    hint: new Text(
                                                        'Nível de Atividade',
                                                        style: TextStyle(
                                                            color:
                                                                color_white)),
                                                    items: <String>[
                                                      'Sedentário',
                                                      'Exercício Leve',
                                                      'Exercício Moderado',
                                                      'Exercício Alto',
                                                      'Atleta Profissional'
                                                    ].map((String valor) {
                                                      return DropdownMenuItem(
                                                          child: Text(valor),
                                                          value: valor);
                                                    }).toList(),
                                                    value: _valorAtividade,
                                                    icon: const Icon(
                                                        Icons.arrow_downward),
                                                    style: TextStyle(
                                                      color: color_white,
                                                    ),
                                                    onChanged:
                                                        (String? valorNovo) {
                                                      setState(() {
                                                        _valorAtividade =
                                                            valorNovo!;
                                                      });
                                                    },
                                                    underline: Container(
                                                        height: 2,
                                                        color: color_orange))),
                                            margin: EdgeInsets.symmetric(
                                                vertical: 10, horizontal: 10)),
                                      ),
                                      Expanded(
                                        child: Container(
                                            child: TextButton(
                                              child: Text('Informações'),
                                              style: TextButton.styleFrom(
                                                  backgroundColor: color_orange,
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 10),
                                                  primary: Colors.black,
                                                  textStyle:
                                                      TextStyle(fontSize: 16),
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              50))),
                                              onPressed: () {
                                                sobreIntensidade();
                                              },
                                            ),
                                            margin: EdgeInsets.symmetric(
                                                vertical: 10, horizontal: 10)),
                                      ),
                                    ],
                                  ),
                                  Row(children: <Widget>[
                                    Expanded(
                                      child: Container(
                                          child: Theme(
                                              data: ThemeData(
                                                  canvasColor: color_dark),
                                              child: DropdownButton<String>(
                                                  isExpanded: true,
                                                  hint: new Text('Objetivo',
                                                      style: TextStyle(
                                                          color: color_white)),
                                                  items: <String>[
                                                    'Perder Peso',
                                                    'Manter Peso',
                                                    'Ganhar Peso'
                                                  ].map((String valor) {
                                                    return DropdownMenuItem(
                                                        child: Text(valor),
                                                        value: valor);
                                                  }).toList(),
                                                  value: _valorObjetivo,
                                                  icon: const Icon(
                                                      Icons.arrow_downward),
                                                  style: TextStyle(
                                                    color: color_white,
                                                  ),
                                                  onChanged:
                                                      (String? valorNovo) {
                                                    setState(() {
                                                      _valorObjetivo =
                                                          valorNovo!;
                                                    });
                                                  },
                                                  underline: Container(
                                                      height: 2,
                                                      color: color_orange))),
                                          margin: EdgeInsets.symmetric(
                                              vertical: 10, horizontal: 10)),
                                    ),
                                    Expanded(
                                      child: Container(
                                          child: TextButton(
                                            child: Text('Informações'),
                                            style: TextButton.styleFrom(
                                                backgroundColor: color_orange,
                                                padding: EdgeInsets.symmetric(
                                                    vertical: 10),
                                                primary: Colors.black,
                                                textStyle:
                                                    TextStyle(fontSize: 16),
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            50))),
                                            onPressed: () {
                                              sobreObjetivo();
                                            },
                                          ),
                                          margin: EdgeInsets.symmetric(
                                              vertical: 10, horizontal: 10)),
                                    ),
                                  ]),
                                ],
                              ),
                            ),
                            /*
================================================Container do Botao================================================
 */
                            Row
                            (
                              children: <Widget>
                              [
                                Container
                                (
                                  child: TextButton(
                                    style: TextButton.styleFrom(
                                        backgroundColor: color_orange,
                                        padding: EdgeInsets.all(20.0),
                                        primary: Colors.black,
                                        textStyle: TextStyle(fontSize: 20),
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(50))),
                                    onPressed: ()
                                    {
                                      passValor();

                                      inputsErrorCheck = 0;
                                      testarTextController(massaMain,"Peso");
                                      testarTextController(alturaMain,"Altura");  
                                      testarTextController(idadeMain,"Idade");  
                                      testarTextController(abdomenMain,"Abdomen");
                                      testarTextController(pescocoMain,"Pescoço");
                                      if(sexo_perfil_g == 1)
                                      {
                                        testarTextController(quadrilMain,"Quadril");
                                      }
                                      
                                      if(inputsErrorCheck == 0)
                                      {
                                        testarInput(double.parse(massaMain.text.toString()), 50, 150, 'kg', 'Peso');
                                        testarInput(double.parse(alturaMain.text.toString()), 145, 210,'cm', 'Altura');
                                        testarInput(double.parse(idadeMain.text.toString()), 18, 65, ' anos', 'Idade');
                                        testarInput(double.parse(abdomenMain.text.toString()), 75, 130, 'cm', 'Circunferência do Abdômen');
                                        testarInput(double.parse(pescocoMain.text.toString()), 35, 46, 'cm',  'Circunferência do Pescoço');
                                        if(sexo_perfil_g == 1)
                                        {
                                          testarInput(double.parse(quadrilMain.text.toString()), 80, 140, 'cm', 'Circunferência do Quadril');
                                        }


                                        if(inputsErrorCheck == 0)
                                        {
                                          calcular().then((value) => save().then((value) => mostrarToastMessage('Os dados foram salvos! 😎')));
                                        }
                                        else
                                        {
                                          mostrarToastMessage('Preencha todos os campos corretamente!');
                                        }
                                      }
                                      else
                                      {
                                        mostrarToastMessage('Preencha todos os campos corretamente!');  
                                      }
                                    },
                                    child: Text(
                                                'Calcular',
                                                style: GoogleFonts.roboto(
                                                  fontSize: 15,
                                                  color: color_black,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                  ),
                                  width: 100,
                                ),


                                Container  
                                (
                                  child: TextButton(
                                    style: TextButton.styleFrom(
                                        backgroundColor: color_white,
                                        padding: EdgeInsets.all(20.0),
                                        primary: Colors.black,
                                        textStyle: TextStyle(fontSize: 20),
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(50))),
                                    onPressed: ()
                                    {
                                      zerarSP();

                                      Navigator.pushReplacement(context, MaterialPageRoute
                                      (
                                        builder: (context) => Login(), 
                                      ));
                                    },
                                    child: Text(
                                                'Sair',
                                                style: GoogleFonts.roboto(
                                                  fontSize: 14,
                                                  color: color_black,
                                                
                                                ),
                                              ),
                                  ),
                                  width: 100,

                                )
                              ],
                              mainAxisAlignment: MainAxisAlignment.spaceBetween
                            ),
                          ],
                        ),
                      ),
                      padding:
                          EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
                      color: color_black,
                      width: double.infinity,
                      margin: EdgeInsets.only(top: 10),
                    ),
                  ),
                  margin: EdgeInsets.only(top: 10),
                ),
                flex: 14),
          ],
        ),
        height: MediaQuery.of(context).size.height,
        color: color_black,
      ),
    );
  }
}
