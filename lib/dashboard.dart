import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:heappy/others/globalVariables.dart';

import './screens/home.dart';
import './screens/perfil.dart';
import 'others/globalFormulas.dart';


class Dashboard extends StatefulWidget
{
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard>
{
  int _selectedIndex = 0;

  final tabs =
  [
    const Center(child: Home()),
    const Center(child: Profile()),
  ];

  @override
  Widget build(BuildContext context)
  {
    return Scaffold
    (
      body: tabs[_selectedIndex],

      bottomNavigationBar: BottomNavigationBar
      (
        currentIndex: _selectedIndex,
        items:
        [
          BottomNavigationBarItem
          (
            icon: Icon(Icons.home),
            label: 'Início',
          ),

          BottomNavigationBarItem
          (
            icon: Icon(Icons.person),
            label: 'Perfil',
            
          )
        ],

        selectedItemColor: color_orange,
        unselectedItemColor: color_light,
        backgroundColor: color_dark,
        type: BottomNavigationBarType.fixed,
        elevation: 0,
        selectedFontSize: 13,
        unselectedFontSize: 13,
        

        onTap:(index)
        {
          setState(()
          {
            _selectedIndex = index;
          });
        },
      ),
    );
  }
}
