import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../others/globalVariables.dart';
import 'dart:math';




Future <void> calcular()async
{
  calcularPI();
  gcPerfil();
  tmbPerfil();
  imcPerfil();
  pmmPerfil();
  calcularPI();
  tdeePerfil();
}


Future zerarSP() async {
  SharedPreferences prefs  = await SharedPreferences.getInstance();

  userNome = "";
  userSenha = "";
  isLogged = 0;

  userPeso = 0.0;
  massa_perfil_g = 0.0;
  altura_perfil_g = 0.0;
  idade_perfil_g = 0.0;
  abdomem_perfil_g = 0.0;
  pescoco_perfil_g = 0.0;
  quadril_perfil_g = 0.0;
  sexo_perfil_g = 0; // M = 1 / F = 2
  dropButton_atividade_perfil_g = 'Sedentário';
  dropButton_objetivo_perfil_g = 'Perder Peso';

  indice = '';
  imc_perfil_g = 0;
  tmb_perfil_g = 0;
  lbm_perfil_g = 0;
  gc_perfil_g = 0;
  tdee_perfil_g = 0;
  tdee_perfil_mensage_g = 'Não informado ainda';
  pmm_perfil_g = 0;
  pmm_perfil_10_g = 0;
  pmm_perfil_15_g = 0;
  min_pi_perfil_g = 0;
  max_pi_perfil_g = 0;

  prefs.setString('nome', '');
  prefs.setString('password', '');
  prefs.setInt('isLogged', 0);

  prefs.setInt("sexo_perfil_g", sexo_perfil_g);
  prefs.setDouble("massa_perfil_g", massa_perfil_g);
  prefs.setDouble("altura_perfil_g", altura_perfil_g);
  prefs.setDouble("idade_perfil_g", idade_perfil_g);
  prefs.setDouble("abdomem_perfil_g", abdomem_perfil_g);
  prefs.setDouble("pescoco_perfil_g", pescoco_perfil_g);
  prefs.setDouble("quadril_perfil_g", quadril_perfil_g);
  prefs.setString("dropButton_atividade_perfil_g", dropButton_atividade_perfil_g);
  prefs.setString("dropButton_objetivo_perfil_g", dropButton_objetivo_perfil_g);

  prefs.setDouble('imc_perfil_g', imc_perfil_g);
  prefs.setDouble('tmb_perfil_g', tmb_perfil_g);
  prefs.setDouble('lbm_perfil_g', lbm_perfil_g);
  prefs.setDouble('gc_perfil_g', gc_perfil_g);
  prefs.setDouble('pmm_perfil_g', pmm_perfil_g);
  prefs.setDouble('pmm_perfil_10_g', pmm_perfil_10_g);
  prefs.setDouble('pmm_perfil_15_g', pmm_perfil_15_g);
  prefs.setDouble('min_pi_perfil_g', min_pi_perfil_g);
  prefs.setDouble('max_pi_perfil_g', max_pi_perfil_g);
  prefs.setDouble('tdee_perfil_g', tdee_perfil_g);
}


Future pegarSPInitial() async
  {
    SharedPreferences prefs  = await SharedPreferences.getInstance();
   
      userNome = (prefs.getString('userNome') ?? "");
      userSenha = (prefs.getString('userSenha') ?? "");
      isLogged = (prefs.getInt('isLogged') ?? 0);
      print("bbbbbbbbbb");
      print(userNome);
  }


Future mostrarToastMessage(String mensagem) async
{
  return Fluttertoast.showToast
  (
    msg: mensagem,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.SNACKBAR,
    fontSize: 15
  );
}


void testarInput(double a, double b, double c, String d, String e)
{
  if(a < b)
  {
    //mostrarToastMessage("As fórmulas não estão projetadas pra funcionarem com valores inferiores a " + c.toStringAsFixed(0) + d + ", inseridos no campo " + e + "! 😕");
    mostrarToastMessage(b.toStringAsFixed(0) + d + " é o valor mínimo pro campo " + e + " 😕");
    inputsErrorCheck = inputsErrorCheck + 1;
  }
  if(a > c)
  {
    //mostrarToastMessage("As fórmulas não estão projetadas pra funcionarem com valores superiores a " + c.toStringAsFixed(0) + d + ", inseridos no campo " + e + "! 😕");
    mostrarToastMessage(c.toStringAsFixed(0) + d + " é o valor máximo pro campo " + e + " 😕");
    inputsErrorCheck = inputsErrorCheck + 1;
  }
}



void testarTextController(TextEditingController a, String b)
{
  TextEditingController temp = TextEditingController();
  //print(a.text);
  if(a.text == temp.text)
  {
    mostrarToastMessage("O campo " + b + " não pode estar vazio! 😕");
    inputsErrorCheck = inputsErrorCheck + 1;
  }
}






void tdeePerfil()
  {
    lbm_perfil_g = (massa_perfil_g - ((massa_perfil_g * gc_perfil_g)/100));

    if(dropButton_atividade_perfil_g == 'Sedentário') //one
    {
      tdee_perfil_g = tmb_perfil_g * 1.195; //1.200109
      print("TDEE 1: " + tdee_perfil_g.toStringAsFixed(0));
    }

    if(dropButton_atividade_perfil_g == 'Exercício Leve') //two
    {
      tdee_perfil_g = tmb_perfil_g * 1.34; //1.374660
      print("TDEE 2: " + tdee_perfil_g.toStringAsFixed(0));
    }

    if(dropButton_atividade_perfil_g == 'Exercício Moderado') //three
    {
      tdee_perfil_g = tmb_perfil_g * 1.5;//1.549755
      print("TDEE 3: " + tdee_perfil_g.toStringAsFixed(0));
    }

    if(dropButton_atividade_perfil_g == 'Exercício Alto') //four
    {
      tdee_perfil_g = tmb_perfil_g * 1.68;//1.724850;
      print("TDEE 4: " + tdee_perfil_g.toStringAsFixed(0));
    }

    if(dropButton_atividade_perfil_g == 'Atleta Profissional') // five
    {
      tdee_perfil_g = tmb_perfil_g * 1.835;//1.899945;
      print("TDEE 5: " + tdee_perfil_g.toStringAsFixed(0));
    }


    if(dropButton_objetivo_perfil_g == 'Perder Peso') //one
    {
      tdee_perfil_g = tdee_perfil_g - (tdee_perfil_g * 0.14);
    }


    if(dropButton_objetivo_perfil_g == 'Ganhar Peso') //three
    {
      tdee_perfil_g = tdee_perfil_g + (tdee_perfil_g * 0.14);
    }

    print("TDEE: " + tdee_perfil_g.toStringAsFixed(0));
  }


void gcPerfil() 

  { 

    if(sexo_perfil_g == 2)

    {

      gc_perfil_g = (495 / ( 1.0324 - 0.19077 * (log(abdomem_perfil_g - pescoco_perfil_g) / log(10)) + 0.15456 * (log(altura_perfil_g) / log(10))) - 450);

    }



    if(sexo_perfil_g == 1)

    { 

      gc_perfil_g = (495 / ( 1.29579 - 0.35004 * (log(abdomem_perfil_g + quadril_perfil_g - pescoco_perfil_g) / log(10)) + 0.22100 * (log(altura_perfil_g) / log(10))) - 450);

    }

  }



void tmbPerfil()

  {

    lbm_perfil_g = (massa_perfil_g - ((massa_perfil_g * gc_perfil_g)/100));

    tmb_perfil_g = (370 + (21.6 * lbm_perfil_g));

  }






void imcPerfil()

  { 

  print(massa_perfil_g );
  print(altura_perfil_g);
    print(imc_perfil_g);
    imc_perfil_g = (massa_perfil_g / (pow(altura_perfil_g, 2)) * 10000);

  }






void pmmPerfil()

  {

    if(altura_perfil_g == 0)

    {

      pmm_perfil_g = 0;

      pmm_perfil_10_g = 0;

      pmm_perfil_15_g = 0;

    }

    else

    {

      pmm_perfil_g = altura_perfil_g - 100;

      pmm_perfil_10_g = pmm_perfil_g + (pmm_perfil_g * 0.05);

      pmm_perfil_15_g = pmm_perfil_g + (pmm_perfil_g * 0.10);

    }

  }



void calcularPI()
{
  if(sexo_perfil_g == 2)
  {
    min_pi_perfil_g = ((altura_perfil_g - 100) * 0.91);
    max_pi_perfil_g = ((altura_perfil_g - 100) * 0.965);
  }

  if(sexo_perfil_g == 1)
  {
    min_pi_perfil_g = ((altura_perfil_g - 100) * 0.85);
    max_pi_perfil_g = ((altura_perfil_g - 100) * 0.882);
  }
}


void welcomeMensage()
{   
  horaEmString = "${now.hour}";
  horaEmDoble = double.parse(horaEmString);
  
  if(horaEmDoble < 6)
  {
    horaMensagem = "Boa noite";
  }
  if(horaEmDoble > 5 && horaEmDoble < 12)
  {
    horaMensagem = "Bom dia";
  }
  if(horaEmDoble > 11 && horaEmDoble < 18)
  {
    horaMensagem = "Boa tarde";
  }
  if(horaEmDoble > 17)
  {
    horaMensagem = "Boa noite";
  }
}