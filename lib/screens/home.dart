import 'package:flutter/material.dart';
import 'package:heappy/others/globalVariables.dart';
import 'package:heappy/others/globalFormulas.dart';


import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';


class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  //double statusBarHeight = MediaQuery.of(context).padding.top;

  Future <void> pegar() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(()
    {
      sexo_perfil_g = (prefs.getInt("sexo") ?? 0);
      massa_perfil_g = (prefs.getDouble('peso') ?? 0.0);
      altura_perfil_g = (prefs.getDouble('altura') ?? 0.0);
      idade_perfil_g = (prefs.getDouble('idade') ?? 0.0);
      abdomem_perfil_g = (prefs.getDouble('abdomen') ?? 0.0);
      pescoco_perfil_g = (prefs.getDouble('pescoco') ?? 0.0);
      quadril_perfil_g = (prefs.getDouble('quadril') ?? 0.0);
      dropButton_atividade_perfil_g = (prefs.getString('nivel_atividade') ?? "");
      dropButton_objetivo_perfil_g = (prefs.getString('objetivo') ?? "");

      imc_perfil_g = (prefs.getDouble('imc_perfil_g') ?? 0);
      tmb_perfil_g = (prefs.getDouble('tmb_perfil_g') ?? 0);
      lbm_perfil_g = (prefs.getDouble('lbm_perfil_g') ?? 0);
      gc_perfil_g = (prefs.getDouble('gc_perfil_g') ?? 0);
      pmm_perfil_g = (prefs.getDouble('pmm_perfil_g') ?? 0);
      pmm_perfil_10_g = (prefs.getDouble('pmm_perfil_10_g') ?? 0);
      pmm_perfil_15_g = (prefs.getDouble('pmm_perfil_15_g') ?? 0);
      min_pi_perfil_g = (prefs.getDouble('min_pi_perfil_g') ?? 0);
      max_pi_perfil_g = (prefs.getDouble('max_pi_perfil_g') ?? 0);
      tdee_perfil_g = (prefs.getDouble('tdee_perfil_g') ?? 0);
    });
  }



  @override
  void initState()
  {
    super.initState();
    pegar();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            Container //Container pra esconder a barra de status
                (
              height: MediaQuery.of(context).padding.top,
              color: color_dark,
            ),
            Expanded(
              child: Container(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        child: RichText(
                          text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                text: "HEA",
                                style: GoogleFonts.roboto(
                                  fontSize: 50,
                                  color: color_light,
                                  fontWeight: FontWeight.w900,
                                  fontStyle: FontStyle.italic,
                                ),
                              ),
                              TextSpan(
                                text: "PPY",
                                style: GoogleFonts.roboto(
                                  fontSize: 50,
                                  color: color_orange,
                                  fontWeight: FontWeight.w900,
                                  fontStyle: FontStyle.italic,
                                ),
                              ),
                            ],
                          ),
                          textAlign: TextAlign.left,
                        ),
                        //height: 60,
                        alignment: Alignment.centerLeft,
                      ),
                      flex: 9,
                    ),
                    Expanded(
                        child: Align(
                          alignment: Alignment.center,
                          child: Container(
                            child: Container(
                              child: IconButton(
                                icon: Icon(
                                  Icons.group,
                                  color: color_light,
                                ),
                                onPressed: () {
                                  //criar um AlertDialog pra mostrar o nome do usuário
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: Text(
                                          "Grupo Stack Underflow ",
                                          style: GoogleFonts.ptSans(
                                            fontSize: 20,
                                            color: color_light,
                                            fontWeight: FontWeight.w600,
                                            //fontStyle: FontStyle.italic,
                                          ),
                                        ),
                                        content: SingleChildScrollView(
                                          child: ListBody(
                                            children: <Widget>[
                                              Text(
                                                '- DANIEL BORBA GUIMARÃES DA COSTA',
                                                style: GoogleFonts.roboto(
                                                  fontSize: 14,
                                                  color: color_light,
                                                ),
                                              ),
                                              Text(
                                                '- DAVID GABRIEL BEILFUSS JORGE',
                                                style: GoogleFonts.roboto(
                                                  fontSize: 14,
                                                  color: color_light,
                                                ),
                                              ),
                                              Text(
                                                '- MURILLO SAMPAIO OLIVEIRA ANTONIO',
                                                style: GoogleFonts.roboto(
                                                  fontSize: 14,
                                                  color: color_light,
                                                ),
                                              ),
                                              Text(
                                                '- OTÁVIO AUGUSTO RIBEIRO ARAÚJO',
                                                style: GoogleFonts.roboto(
                                                  fontSize: 14,
                                                  color: color_light,
                                                ),
                                              ),
                                              Text(
                                                '- SÔNIA RESENDE DE ARAÚJO',
                                                style: GoogleFonts.roboto(
                                                  fontSize: 14,
                                                  color: color_light,
                                                ),
                                              ),
                                              Text(
                                                '- THAÍS RESENDE ARAÚJO BORGES BONFIM',
                                                style: GoogleFonts.roboto(
                                                  fontSize: 14,
                                                  color: color_light,
                                                ),
                                              ),
                                              Text(
                                                '',
                                                style: TextStyle(
                                                    fontFamily: 'ProductSans',
                                                    fontSize: 15),
                                              ),
                                              

                                              GestureDetector
                                              (
                                                onTap: () => launch('https://gitlab.com/otavio123ribeiro/heappy'),
                                                child: Card
                                                (
                                                  child: Row
                                                  (
                                                    children: <Widget>
                                                    [
                                                      Expanded
                                                      (
                                                        flex: 3, // 60%
                                                        child: IconButton
                                                        (
                                                          icon: Image.asset("assets/icon/interface/gitlab.png"),
                                                          onPressed: () {},
                                                        ),
                                                      ),
                                                      
                                                      Expanded
                                                      (
                                                        flex: 21, // 60%
                                                        child: Text
                                                        (
                                                          "GitLab Link",
                                                          style: GoogleFonts.roboto
                                                          (
                                                            fontSize: 14,
                                                            color: color_black,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  shape: RoundedRectangleBorder
                                                  (
                                                    borderRadius: BorderRadius.circular(30.0),
                                                  ),
                                                  color: color_light,
                                                ),
                                              ),
                                              
                                              
                                              Text(
                                                '',
                                                style: TextStyle(
                                                    fontFamily: 'ProductSans',
                                                    fontSize: 15),
                                              ),
                                              Text(
                                                'A ideia principal do aplicativo é realizar cálculos através de valores informados pelo usuário. Alguns deles são: porcentagem de gordura corporal, índice de massa corporal, taxa metabólica basal, gasto calórico diário, peso ideal, potencial muscular máximo...  Até o momento está previsto ao menos 4 "telas", onde 1 será uma provável tela de login, 1 será para o usuário inserir/atualizar suas informações, 1 será para mostrar os resultados calculados. Outras "telas" também podem ser inseridas futuramente. Os valores necessários para realizar os cálculos serão por exemplo: altura, idade, peso, sexo... Com essas informações, será possível realizar cálculos através de funções.Se faz necessário ressaltar que a aplicação terá validação médica, sendo a palavra de um responsável como final para quaisquer fins. No entanto, serão utilizados fórmulas e métodos muito conhecidos como as fórmulas de Mifflin-St Jeor, a equação revisada de Harris-Benedict. Ainda não estão definidos quais serão as bibliotecas, ou pacotes externos serão utilizados, no entanto, provavelmente serão necessárias soluções para salvamento de informações de forma local, exibição de gráficos (se for decidido sua implementação),  uma forma simples de mostrar avisos (possivelmente através de "toast notifications"). Já está sendo analisada a possibilidade de uso de um banco de dados, mas nada definido, tendo em vista que o prazo para desenvolvimento é curto e outros fatores.',
                                                style: GoogleFonts.roboto(
                                                  fontSize: 14,
                                                  color: color_light,
                                                ),
                                              ),
                                              Text(
                                                '',
                                                style: TextStyle(
                                                    fontFamily: 'ProductSans',
                                                    fontSize: 15),
                                              ),
                                              Text(
                                                'É importante para nós que os usuários permaneçam saudáveis enquanto atingem suas metas de condicionamento físico e bem-estar. Seja responsável e use seu melhor julgamento e bom senso. Fornecemos nossos serviços apenas para fins informativos e não podemos ser responsabilizados em caso de lesão ou se caso apresentar um problema de saúde. Em particular, embora a maior parte do conteúdo publicado pelos outros usuários em nossa comunidade seja útil, ela é proveniente de estranhos na Internet e nunca deve prevalecer sobre o bom senso ou o aconselhamento médico real. Ao usar nossos Serviços, você concorda, representa e garante que recebeu o consentimento do seu médico para participar dos Programas ou de qualquer uma das atividades relacionadas disponibilizadas para você em conexão com os Serviços. Além disso, você concorda, declara e garante que consultou seu médico antes de fazer qualquer alteração alimentar com base nas informações disponíveis através dos Serviços. As condições e habilidades de todos são diferentes, e participar dos Programas e outras atividades promovidas por nossos Serviços é por sua conta e risco. Se você optar por participar desses Programas e outras atividades, você o faz por sua livre e espontânea vontade, concordando, consciente e voluntariamente, assumindo todos os riscos associados a tais atividades. Os programas e outras atividades promovidas pelos Serviços podem representar riscos mesmo para aqueles que estão atualmente com boa saúde.',
                                                style: GoogleFonts.roboto
                                                (
                                                  fontSize: 14,
                                                  color: color_light,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: Text(
                                              "Fechar",
                                              style: GoogleFonts.roboto(
                                                fontSize: 15,
                                                color: color_orange,
                                                //fontWeight: FontWeight.w800,
                                                //fontStyle: FontStyle.italic,
                                              ),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                        backgroundColor: color_dark,
                                      );
                                    },
                                  );
                                },
                              ),
                              decoration: BoxDecoration(
                                  color: color_dark,
                                  border:
                                      Border.all(color: color_orange, width: 1),
                                  //border: Border.all(color: color_orange, width: 5.0),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(35.0)),
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/wavey-fingerprint.png'),
                                      fit: BoxFit.cover)),
                              margin: EdgeInsets.only(right: 20),
                              height: 38,
                              //width: ,
                              alignment: Alignment.center,
                            ),
                          ),
                        ),
                        flex: 2),
                  ],
                ),

                //Container com as bordas de baixo redondas
                decoration: BoxDecoration(
                  color: color_dark,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(35),
                    bottomRight: Radius.circular(35),
                  ),
                ),
                width: double.infinity,
                padding: EdgeInsets.only(left: 20),
              ),
              flex: 2,
            ),
            Expanded(
                child: Container(
                  child: SingleChildScrollView(
                    child: Container(
                      child: Container(
                        child: Column(
                          children: <Widget>
                          [
/*
===================================Container do IMC===================================
*/
                            /*
===================================GC===================================
*/

                            Container 
                            (
                              child: Column
                              (
                                children: <Widget>
                                [
                                  Expanded
                                  (
                                    child: Row
                                    (
                                      children: <Widget>
                                      [
                                        Expanded
                                        (
                                          child: Center
                                          (
                                            child: Container
                                            (
                                              child: Image.asset('assets/icon/interface/caliper.png', color: Color.fromRGBO(34, 40, 49,1),),
                                              width: 35,
                                              height: 35,
                                              padding: const EdgeInsets.all(8),

                                              decoration: BoxDecoration
                                              (
                                                borderRadius: BorderRadius.all(Radius.circular(100)),
                                                color: color_orange,
                                              ),
                                            ),
                                          ),
                                          flex: 5,
                                        ),

                                        Expanded
                                        (
                                          child: Container
                                          (
                                            child: Text("Gordura Corporal",
                                            style: GoogleFonts.roboto
                                            (
                                              fontSize: 20,
                                              color: color_black,
                                              fontWeight: FontWeight.w900,
                                              fontStyle: FontStyle.italic,
                                            ),
                                            textAlign: TextAlign.left),
                                          ),
                                          flex: 30,
                                        ),
                                      ],
                                    ),
                                    flex: 2,
                                  ),

                                  Expanded
                                  (
                                    child: Center
                                    (
                                      child:Text
                                      (
                                        "${gc_perfil_g.toStringAsFixed(0)}%",
                                        style: GoogleFonts.roboto
                                        (
                                          fontSize: 25,
                                          color: color_black,
                                          fontWeight: FontWeight.w900,
                                        ),
                                      ),
                                    ),
                                    flex: 3,
                                  ),
                                  
                                  Expanded
                                  (
                                    child: Row
                                    (
                                      children: <Widget>
                                      [
                                        Expanded
                                        (
                                          child: Container
                                          (
                                            child: Text
                                            (
                                              " Informação do perfil",
                                              style: GoogleFonts.roboto
                                              (
                                                fontSize: 15,
                                                color: color_black,
                                              ),
                                              textAlign: TextAlign.left
                                            ),
                                            margin: EdgeInsets.only(left: 5),
                                          ),
                                          flex: 7,
                                        ),

                                        Expanded
                                        (
                                          child: Container
                                          (
                                            margin: const EdgeInsets.only(right: 3.0),
                                            child: IconButton
                                            (
                                              icon: new Icon(Icons.assignment_late_outlined, color: color_black,),
                                              onPressed: ()
                                              {
                                                showDialog
                                                (
                                                  context: context,
                                                  builder: (BuildContext context)
                                                  {
                                                    return AlertDialog
                                                    (
                                                      title: Text('Sobre', 
                                                      style: GoogleFonts.ptSans(
                                                        fontSize: 20,
                                                        color: color_light,
                                                        fontWeight: FontWeight.w600,
                                                        //fontStyle: FontStyle.italic,
                                                      ),
                                                      ),
                                                      content: SingleChildScrollView
                                                      (
                                                        child: Column
                                                        (
                                                          children: 
                                                          [
                                                            Text('O peso ideal é o peso que a pessoa deve ter para a sua altura, sendo isso importante para evitar complicações como obesidade, hipertensão e diabetes ou até mesmo a desnutrição, quando a pessoa está muito abaixo do peso. ',
                                                            style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                            textAlign: TextAlign.center ,),
                                                            Text('', style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                            textAlign: TextAlign.center ,),
                                                            Text('É importante mencionar que como utiliza o resultado do IMC para ser calculado, não leva em consideração a quantidade de gordura, de músculo ou de água que a pessoa apresenta, sendo apenas uma referência de peso para a altura da pessoa. Por isso, se uma pessoa possui muita massa muscular ou apresenta retenção de líquidos, o peso ideal indica que o IMC poderá não ser o mais adequado, sendo necessário, nesses casos, realizar uma avaliação nutricional.', style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                            textAlign: TextAlign.center ,),
                                                            SizedBox(height: 10),
                                                          ],
                                                        ),
                                                      ),
                                                      actions:
                                                      [                                
                                                        FlatButton
                                                        (
                                                          child: Text("Fechar",
                                                          style: GoogleFonts.roboto
                                                          (
                                                            fontSize: 15,
                                                            color: color_orange,
                                                            fontWeight: FontWeight.w800,
                                                            fontStyle: FontStyle.italic,
                                                          ),
                                                          ),
                                                          onPressed:()
                                                          {
                                                            Navigator.of(context).pop();
                                                          },
                                                        )
                                                      ], // Action
                                                      backgroundColor: color_dark,
                                                    );
                                                  }
                                                );
                                              },
                                            ),
                                            alignment: Alignment.centerRight, 
                                          ),
                                          flex: 3,
                                        ), 
                                      ],
                                    ), 
                                    flex: 2
                                  ),
                                ],
                              ),
                              
                              decoration: BoxDecoration
                              (
                                borderRadius: BorderRadius.all(Radius.circular(15)),
                                color: color_light,
                                border: Border.all(width: 2, color: color_orange,),
                              ),

                              width: double.infinity,
                              margin: const EdgeInsets.all(15),
                              height: 150,
                            ),




/*
===================================Gasto Energético Diário===================================
*/
                            Container 
                            (
                              child: Column
                              (
                                children: <Widget>
                                [
                                  Expanded
                                  (
                                    child: Row
                                    (
                                      children: <Widget>
                                      [
                                        Expanded
                                        (
                                          child: Center
                                          (
                                            child: Container
                                            (
                                              child: Image.asset('assets/icon/interface/flame.png', color: Color.fromRGBO(34, 40, 49,1),),
                                              width: 35,
                                              height: 35,
                                              padding: const EdgeInsets.all(8),

                                              decoration: BoxDecoration
                                              (
                                                borderRadius: BorderRadius.all(Radius.circular(100)),
                                                color: color_orange,
                                              ),
                                            ),
                                          ),
                                          flex: 5,
                                        ),

                                        Expanded
                                        (
                                          child: Container
                                          (
                                            child: Text("Gasto Energético Diário",
                                            style: GoogleFonts.roboto
                                            (
                                              fontSize: 20,
                                              color: color_black,
                                              fontWeight: FontWeight.w900,
                                              fontStyle: FontStyle.italic,
                                            ),
                                            textAlign: TextAlign.left),
                                          ),
                                          flex: 30,
                                        ),
                                      ],
                                    ),
                                    flex: 2,
                                  ),

                                  Expanded
                                  (
                                    child: Center
                                    (
                                      child:Text
                                      (
                                        "${tdee_perfil_g.toStringAsFixed(0)}kcal/dia",
                                        style: GoogleFonts.roboto
                                        (
                                          fontSize: 23,
                                          color: color_black,
                                          fontWeight: FontWeight.w800,
                                        ),
                                      ),
                                    ),
                                    flex: 3,
                                  ),
                                  
                                  Expanded
                                  (
                                    child: Row
                                    (
                                      children: <Widget>
                                      [
                                        Expanded
                                        (
                                          child: Container
                                          (
                                            child: Text
                                            (
                                              " Informação do perfil",
                                              style: GoogleFonts.roboto
                                              (
                                                fontSize: 15,
                                                color: color_black,
                                              ),
                                              textAlign: TextAlign.left
                                            ),
                                            margin: EdgeInsets.only(left: 5),
                                          ),
                                          flex: 7,
                                        ),

                                        Expanded
                                        (
                                          child: Container
                                          (
                                            margin: const EdgeInsets.only(right: 3.0),
                                            child: IconButton
                                            (
                                              icon: new Icon(Icons.assignment_late_outlined, color: color_black,),
                                              onPressed: ()
                                              {
                                                showDialog
                                                (
                                                  context: context,
                                                  builder: (BuildContext context)
                                                  {
                                                    return AlertDialog
                                                    (
                                                      title: Text('Sobre', 
                                                      style: GoogleFonts.ptSans(
                                                        fontSize: 20,
                                                        color: color_light,
                                                        fontWeight: FontWeight.w600,
                                                        //fontStyle: FontStyle.italic,
                                                      ),
                                                      ),
                                                      content: SingleChildScrollView
                                                      (
                                                        child: Column
                                                        (
                                                          children: 
                                                          [
                                                            Text('O peso ideal é o peso que a pessoa deve ter para a sua altura, sendo isso importante para evitar complicações como obesidade, hipertensão e diabetes ou até mesmo a desnutrição, quando a pessoa está muito abaixo do peso. ',
                                                            style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                            textAlign: TextAlign.center ,),
                                                            Text('', style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                            textAlign: TextAlign.center ,),
                                                            Text('É importante mencionar que como utiliza o resultado do IMC para ser calculado, não leva em consideração a quantidade de gordura, de músculo ou de água que a pessoa apresenta, sendo apenas uma referência de peso para a altura da pessoa. Por isso, se uma pessoa possui muita massa muscular ou apresenta retenção de líquidos, o peso ideal indica que o IMC poderá não ser o mais adequado, sendo necessário, nesses casos, realizar uma avaliação nutricional.', style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                            textAlign: TextAlign.center ,),
                                                            SizedBox(height: 10),
                                                          ],
                                                        ),
                                                      ),
                                                      actions:
                                                      [                                
                                                        FlatButton
                                                        (
                                                          child: Text("Fechar",
                                                          style: GoogleFonts.roboto
                                                          (
                                                            fontSize: 15,
                                                            color: color_orange,
                                                            fontWeight: FontWeight.w800,
                                                            fontStyle: FontStyle.italic,
                                                          ),
                                                          ),
                                                          onPressed:()
                                                          {
                                                            Navigator.of(context).pop();
                                                          },
                                                        )
                                                      ], // Action
                                                      backgroundColor: color_dark,
                                                    );
                                                  }
                                                );
                                              },
                                            ),
                                            alignment: Alignment.centerRight, 
                                          ),
                                          flex: 3,
                                        ), 
                                      ],
                                    ), 
                                    flex: 2
                                  ),
                                ],
                              ),
                              
                              decoration: BoxDecoration
                              (
                                borderRadius: BorderRadius.all(Radius.circular(15)),
                                color: color_light,
                                border: Border.all(width: 2, color: color_orange,),
                              ),

                              width: double.infinity,
                              margin: const EdgeInsets.all(15),
                              height: 150,
                            ),

                        
/*
===================================Peso ideal===================================
*/

                            Container 
                            (
                              child: Column
                              (
                                children: <Widget>
                                [
                                  Expanded
                                  (
                                    child: Row
                                    (
                                      children: <Widget>
                                      [
                                        Expanded
                                        (
                                          child: Center
                                          (
                                            child: Container
                                            (
                                              child: Image.asset('assets/icon/interface/balança.png', color: Color.fromRGBO(34, 40, 49,1),),
                                              width: 35,
                                              height: 35,
                                              padding: const EdgeInsets.all(8),

                                              decoration: BoxDecoration
                                              (
                                                borderRadius: BorderRadius.all(Radius.circular(100)),
                                                color: color_orange,
                                              ),
                                            ),
                                          ),
                                          flex: 5,
                                        ),

                                        Expanded
                                        (
                                          child: Container
                                          (
                                            child: Text("Peso Ideal",
                                            style: GoogleFonts.roboto
                                            (
                                              fontSize: 20,
                                              color: color_black,
                                              fontWeight: FontWeight.w900,
                                              fontStyle: FontStyle.italic,
                                            ),
                                            textAlign: TextAlign.left),
                                          ),
                                          flex: 30,
                                        ),
                                      ],
                                    ),
                                    flex: 2,
                                  ),

                                  Expanded
                                  (
                                    child: Center
                                    (
                                      child:Text
                                      (
                                        "Entre ${min_pi_perfil_g.toStringAsFixed(0)}kg e ${max_pi_perfil_g.toStringAsFixed(0)}kg",
                                        style: GoogleFonts.roboto
                                        (
                                          fontSize: 23,
                                          color: color_black,
                                          fontWeight: FontWeight.w800,
                                        ),
                                      ),
                                    ),
                                    flex: 3,
                                  ),
                                  
                                  Expanded
                                  (
                                    child: Row
                                    (
                                      children: <Widget>
                                      [
                                        Expanded
                                        (
                                          child: Container
                                          (
                                            child: Text
                                            (
                                              " Informação do perfil",
                                              style: GoogleFonts.roboto
                                              (
                                                fontSize: 15,
                                                color: color_black,
                                              ),
                                              textAlign: TextAlign.left
                                            ),
                                            margin: EdgeInsets.only(left: 5),
                                          ),
                                          flex: 7,
                                        ),

                                        Expanded
                                        (
                                          child: Container
                                          (
                                            margin: const EdgeInsets.only(right: 3.0),
                                            child: IconButton
                                            (
                                              icon: new Icon(Icons.assignment_late_outlined, color: color_black,),
                                              onPressed: ()
                                              {
                                                showDialog
                                                (
                                                  context: context,
                                                  builder: (BuildContext context)
                                                  {
                                                    return AlertDialog
                                                    (
                                                      title: Text('Sobre', 
                                                      style: GoogleFonts.ptSans(
                                                        fontSize: 20,
                                                        color: color_light,
                                                        fontWeight: FontWeight.w600,
                                                        //fontStyle: FontStyle.italic,
                                                      ),
                                                      ),
                                                      content: SingleChildScrollView
                                                      (
                                                        child: Column
                                                        (
                                                          children: 
                                                          [
                                                            Text('O peso ideal é o peso que a pessoa deve ter para a sua altura, sendo isso importante para evitar complicações como obesidade, hipertensão e diabetes ou até mesmo a desnutrição, quando a pessoa está muito abaixo do peso. ',
                                                            style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                            textAlign: TextAlign.center ,),
                                                            Text('', style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                            textAlign: TextAlign.center ,),
                                                            Text('É importante mencionar que como utiliza o resultado do IMC para ser calculado, não leva em consideração a quantidade de gordura, de músculo ou de água que a pessoa apresenta, sendo apenas uma referência de peso para a altura da pessoa. Por isso, se uma pessoa possui muita massa muscular ou apresenta retenção de líquidos, o peso ideal indica que o IMC poderá não ser o mais adequado, sendo necessário, nesses casos, realizar uma avaliação nutricional.', style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                            textAlign: TextAlign.center ,),
                                                            SizedBox(height: 10),
                                                          ],
                                                        ),
                                                      ),
                                                      actions:
                                                      [                                
                                                        FlatButton
                                                        (
                                                          child: Text("Fechar",
                                                          style: GoogleFonts.roboto
                                                          (
                                                            fontSize: 15,
                                                            color: color_orange,
                                                            fontWeight: FontWeight.w800,
                                                            fontStyle: FontStyle.italic,
                                                          ),
                                                          ),
                                                          onPressed:()
                                                          {
                                                            Navigator.of(context).pop();
                                                          },
                                                        )
                                                      ], // Action
                                                      backgroundColor: color_dark,
                                                    );
                                                  }
                                                );
                                              },
                                            ),
                                            alignment: Alignment.centerRight, 
                                          ),
                                          flex: 3,
                                        ), 
                                      ],
                                    ), 
                                    flex: 2
                                  ),
                                ],
                              ),
                              
                              decoration: BoxDecoration
                              (
                                borderRadius: BorderRadius.all(Radius.circular(15)),
                                color: color_light,
                                border: Border.all(width: 2, color: color_orange,),
                              ),

                              width: double.infinity,
                              margin: const EdgeInsets.all(15),
                              height: 150,
                            ),

            /*
===================================IMC===================================
*/


Container //Potêncial máximo muscular
                            (
                              child: Column
                              (
                                children: <Widget>
                                [
                                  Expanded
                                  (
                                    child: Row
                                    (
                                      children: <Widget>
                                      [
                                        Expanded
                                        (
                                          child: Center
                                          (
                                            child: Container
                                            (
                                              child: Image.asset('assets/icon/interface/biceps.png', color: Colors.black),
                                              width: 35,
                                              height: 35,
                                              padding: const EdgeInsets.all(8),

                                              decoration: BoxDecoration
                                              (
                                                borderRadius: BorderRadius.all(Radius.circular(100)),
                                                color: color_orange,
                                              ),
                                            ),
                                          ),
                                          flex: 5,
                                        ),

                                        Expanded
                                        (
                                          child: Container
                                          (
                                            child: Text("Potencial Muscular Máximo",
                                            style: GoogleFonts.roboto
                                            (
                                              fontSize: 20,
                                              color: color_black,
                                              fontWeight: FontWeight.w900,
                                              fontStyle: FontStyle.italic,
                                            ),
                                            textAlign: TextAlign.left),
                                          ),
                                          flex: 30,
                                        ),
                                      ],
                                    ),
                                    flex: 2,
                                  ),

                                  Expanded
                                  (
                                    child: Center
                                    (
                                      child: Center
                                      (
                                        child:Column
                                        (
                                          children: <Widget>
                                          [
                                            Text("${pmm_perfil_g.toStringAsFixed(0)} kg a 5% de gordura",
                                            style: GoogleFonts.roboto
                                            (
                                              fontSize: 21,
                                              color: color_black,
                                              fontWeight: FontWeight.w800,
                                            ),
                                            textAlign: TextAlign.center),
                                            Text("${pmm_perfil_10_g.toStringAsFixed(0)} kg a 10% de gordura",
                                            style: GoogleFonts.roboto
                                            (
                                              fontSize: 21,
                                              color: color_black,
                                              fontWeight: FontWeight.w800,
                                            ),
                                            textAlign: TextAlign.center),
                                            Text("${pmm_perfil_15_g.toStringAsFixed(0)} kg a 15% de gordura",
                                            style: GoogleFonts.roboto
                                            (
                                              fontSize: 21,
                                              color: color_black,
                                              fontWeight: FontWeight.w800,
                                            ),
                                            textAlign: TextAlign.center),
                                          ],
                                        )
                                      ),
                                    ),
                                    flex: 3,
                                  ),
                                  
                                  Expanded
                                  (
                                    child: Row
                                    (
                                      children: <Widget>
                                      [
                                        Expanded
                                        (
                                          child: Container
                                          (
                                            child: Text
                                            (
                                              " Informação do perfil",
                                              style: GoogleFonts.roboto
                                              (
                                                fontSize: 15,
                                                color: color_black,
                                              ),
                                              textAlign: TextAlign.left
                                            ),
                                            margin: EdgeInsets.only(left: 5),
                                          ),
                                          flex: 7,
                                        ),

                                        Expanded
                                        (
                                          child: Container
                                          (
                                            margin: const EdgeInsets.only(right: 3.0),
                                            child: IconButton
                                            (
                                              icon: new Icon(Icons.assignment_late_outlined, color: color_black,),
                                              onPressed: ()
                                              {
                                                showDialog
                                                (
                                                  context: context,
                                                  builder: (BuildContext context)
                                                  {
                                                    return AlertDialog
                                                    (
                                                      title: Text('Sobre', 
                                                      style: GoogleFonts.ptSans(
                                                        fontSize: 20,
                                                        color: color_light,
                                                        fontWeight: FontWeight.w600,
                                                        //fontStyle: FontStyle.italic,
                                                      ),
                                                      ),
                                                      content: SingleChildScrollView
                                                      (
                                                        child: Column
                                                        (
                                                          children: 
                                                          [
                                                            Text('O potencial muscular máximo (PMM) é um conceito que demonstra o quanto seus músculos podem crescer. No entanto, pode não funcionar para pessoas do sexo feminino.',
                                                            style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                            textAlign: TextAlign.center ,),
                                                            SizedBox(height: 10),
                                                          ],
                                                        ),
                                                      ),
                                                      actions:
                                                      [                                
                                                        FlatButton
                                                        (
                                                          child: Text("Fechar",
                                                          style: GoogleFonts.roboto
                                                          (
                                                            fontSize: 15,
                                                            color: color_orange,
                                                            fontWeight: FontWeight.w800,
                                                            fontStyle: FontStyle.italic,
                                                          ),
                                                          ),
                                                          onPressed:()
                                                          {
                                                            Navigator.of(context).pop();
                                                          },
                                                        )
                                                      ], // Action
                                                      backgroundColor: color_dark,
                                                    );
                                                  }
                                                );
                                              },
                                            ),
                                            alignment: Alignment.centerRight, 
                                          ),
                                          flex: 3,
                                        ), 
                                      ],
                                    ), 
                                    flex: 2
                                  ),
                                ],
                              ),
                              
                              decoration: BoxDecoration
                              (
                                borderRadius: BorderRadius.all(Radius.circular(15)),
                                color: color_light,
                                border: Border.all(width: 2, color: color_orange,),
                              ),

                              width: double.infinity,
                              margin: const EdgeInsets.all(15),
                              height: 180,
                            ),

                            Container 
                            (
                              child: Column
                              (
                                children: <Widget>
                                [
                                  Expanded
                                  (
                                    child: Row
                                    (
                                      children: <Widget>
                                      [
                                        Expanded
                                        (
                                          child: Center
                                          (
                                            child: Container
                                            (
                                              child: Image.asset('assets/icon/interface/weight-scale.png', color: Color.fromRGBO(34, 40, 49,1),),
                                              width: 35,
                                              height: 35,
                                              padding: const EdgeInsets.all(8),

                                              decoration: BoxDecoration
                                              (
                                                borderRadius: BorderRadius.all(Radius.circular(100)),
                                                color: color_orange,
                                              ),
                                            ),
                                          ),
                                          flex: 5,
                                        ),

                                        Expanded
                                        (
                                          child: Container
                                          (
                                            child: Text("Índice de Massa Corporal",
                                            style: GoogleFonts.roboto
                                            (
                                              fontSize: 20,
                                              color: color_black,
                                              fontWeight: FontWeight.w900,
                                              fontStyle: FontStyle.italic,
                                            ),
                                            textAlign: TextAlign.left),
                                          ),
                                          flex: 30,
                                        ),
                                      ],
                                    ),
                                    flex: 2,
                                  ),

                                  Expanded
                                  (
                                    child: Center
                                    (
                                      child:Text
                                      (
                                        "${imc_perfil_g.toStringAsFixed(1)}",
                                        style: GoogleFonts.roboto
                                        (
                                          fontSize: 23,
                                          color: color_black,
                                          fontWeight: FontWeight.w800,
                                        ),
                                      ),
                                    ),
                                    flex: 3,
                                  ),
                                  
                                  Expanded
                                  (
                                    child: Row
                                    (
                                      children: <Widget>
                                      [
                                        Expanded
                                        (
                                          child: Container
                                          (
                                            child: Text
                                            (
                                              " Informação do perfil",
                                              style: GoogleFonts.roboto
                                              (
                                                fontSize: 15,
                                                color: color_black,
                                              ),
                                              textAlign: TextAlign.left
                                            ),
                                            margin: EdgeInsets.only(left: 5),
                                          ),
                                          flex: 7,
                                        ),

                                        Expanded
                                        (
                                          child: Container
                                          (
                                            margin: const EdgeInsets.only(right: 3.0),
                                            child: IconButton
                                            (
                                              icon: new Icon(Icons.assignment_late_outlined, color: color_black,),
                                              onPressed: ()
                                              {
                                                showDialog
                                                (
                                                  context: context,
                                                  builder: (BuildContext context)
                                                  {
                                                    return AlertDialog
                                                    (
                                                      title: Text('Sobre', 
                                                      style: GoogleFonts.ptSans(
                                                        fontSize: 20,
                                                        color: color_light,
                                                        fontWeight: FontWeight.w600,
                                                        //fontStyle: FontStyle.italic,
                                                      ),
                                                      ),
                                                      content: SingleChildScrollView
                                                      (
                                                        child: Column
                                                        (
                                                          children: 
                                                          [
                                                            Text('O peso ideal é o peso que a pessoa deve ter para a sua altura, sendo isso importante para evitar complicações como obesidade, hipertensão e diabetes ou até mesmo a desnutrição, quando a pessoa está muito abaixo do peso. ',
                                                            style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                            textAlign: TextAlign.center ,),
                                                            Text('', style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                            textAlign: TextAlign.center ,),
                                                            Text('É importante mencionar que como utiliza o resultado do IMC para ser calculado, não leva em consideração a quantidade de gordura, de músculo ou de água que a pessoa apresenta, sendo apenas uma referência de peso para a altura da pessoa. Por isso, se uma pessoa possui muita massa muscular ou apresenta retenção de líquidos, o peso ideal indica que o IMC poderá não ser o mais adequado, sendo necessário, nesses casos, realizar uma avaliação nutricional.', style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                            textAlign: TextAlign.center ,),
                                                            SizedBox(height: 10),
                                                          ],
                                                        ),
                                                      ),
                                                      actions:
                                                      [                                
                                                        FlatButton
                                                        (
                                                          child: Text("Fechar",
                                                          style: GoogleFonts.roboto
                                                          (
                                                            fontSize: 15,
                                                            color: color_orange,
                                                            fontWeight: FontWeight.w800,
                                                            fontStyle: FontStyle.italic,
                                                          ),
                                                          ),
                                                          onPressed:()
                                                          {
                                                            Navigator.of(context).pop();
                                                          },
                                                        )
                                                      ], // Action
                                                      backgroundColor: color_dark,
                                                    );
                                                  }
                                                );
                                              },
                                            ),
                                            alignment: Alignment.centerRight, 
                                          ),
                                          flex: 3,
                                        ), 
                                      ],
                                    ), 
                                    flex: 2
                                  ),
                                ],
                              ),
                              
                              decoration: BoxDecoration
                              (
                                borderRadius: BorderRadius.all(Radius.circular(15)),
                                color: color_light,
                                border: Border.all(width: 2, color: color_orange,),
                              ),

                              width: double.infinity,
                              margin: const EdgeInsets.all(15),
                              height: 150,
                            ),              

                                                 
/*
===================================TMB===================================
*/

                            Container 
                            (
                              child: Column
                              (
                                children: <Widget>
                                [
                                  Expanded
                                  (
                                    child: Row
                                    (
                                      children: <Widget>
                                      [
                                        Expanded
                                        (
                                          child: Center
                                          (
                                            child: Container
                                            (
                                              child: Image.asset('assets/icon/interface/metabolismo.png', color: Color.fromRGBO(34, 40, 49,1),),
                                              width: 35,
                                              height: 35,
                                              padding: const EdgeInsets.all(8),

                                              decoration: BoxDecoration
                                              (
                                                borderRadius: BorderRadius.all(Radius.circular(100)),
                                                color: color_orange,
                                              ),
                                            ),
                                          ),
                                          flex: 5,
                                        ),

                                        Expanded
                                        (
                                          child: Container
                                          (
                                            child: Text("Taxa Metabólica Basal",
                                            style: GoogleFonts.roboto
                                            (
                                              fontSize: 20,
                                              color: color_black,
                                              fontWeight: FontWeight.w900,
                                              fontStyle: FontStyle.italic,
                                            ),
                                            textAlign: TextAlign.left),
                                          ),
                                          flex: 30,
                                        ),
                                      ],
                                    ),
                                    flex: 2,
                                  ),

                                  Expanded
                                  (
                                    child: Center
                                    (
                                      child:Text
                                      (
                                        "${tmb_perfil_g.toStringAsFixed(0)}kcal/dia",
                                        style: GoogleFonts.roboto
                                        (
                                          fontSize: 23,
                                          color: color_black,
                                          fontWeight: FontWeight.w800,
                                        ),
                                      ),
                                    ),
                                    flex: 3,
                                  ),
                                  
                                  Expanded
                                  (
                                    child: Row
                                    (
                                      children: <Widget>
                                      [
                                        Expanded
                                        (
                                          child: Container
                                          (
                                            child: Text
                                            (
                                              " Informação do perfil",
                                              style: GoogleFonts.roboto
                                              (
                                                fontSize: 15,
                                                color: color_black,
                                              ),
                                              textAlign: TextAlign.left
                                            ),
                                            margin: EdgeInsets.only(left: 5),
                                          ),
                                          flex: 7,
                                        ),

                                        Expanded
                                        (
                                          child: Container
                                          (
                                            margin: const EdgeInsets.only(right: 3.0),
                                            child: IconButton
                                            (
                                              icon: new Icon(Icons.assignment_late_outlined, color: color_black,),
                                              onPressed: ()
                                              {
                                                showDialog
                                                (
                                                  context: context,
                                                  builder: (BuildContext context)
                                                  {
                                                    return AlertDialog
                                                    (
                                                      title: Text('Sobre', 
                                                      style: GoogleFonts.ptSans(
                                                        fontSize: 20,
                                                        color: color_light,
                                                        fontWeight: FontWeight.w600,
                                                        //fontStyle: FontStyle.italic,
                                                      ),
                                                      ),
                                                      content: SingleChildScrollView
                                                      (
                                                        child: Column
                                                        (
                                                          children: 
                                                          [
                                                            Text('O peso ideal é o peso que a pessoa deve ter para a sua altura, sendo isso importante para evitar complicações como obesidade, hipertensão e diabetes ou até mesmo a desnutrição, quando a pessoa está muito abaixo do peso. ',
                                                            style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                            textAlign: TextAlign.center ,),
                                                            Text('', style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                            textAlign: TextAlign.center ,),
                                                            Text('É importante mencionar que como utiliza o resultado do IMC para ser calculado, não leva em consideração a quantidade de gordura, de músculo ou de água que a pessoa apresenta, sendo apenas uma referência de peso para a altura da pessoa. Por isso, se uma pessoa possui muita massa muscular ou apresenta retenção de líquidos, o peso ideal indica que o IMC poderá não ser o mais adequado, sendo necessário, nesses casos, realizar uma avaliação nutricional.', style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                            textAlign: TextAlign.center ,),
                                                            SizedBox(height: 10),
                                                          ],
                                                        ),
                                                      ),
                                                      actions:
                                                      [                                
                                                        FlatButton
                                                        (
                                                          child: Text("Fechar",
                                                          style: GoogleFonts.roboto
                                                          (
                                                            fontSize: 15,
                                                            color: color_orange,
                                                            fontWeight: FontWeight.w800,
                                                            fontStyle: FontStyle.italic,
                                                          ),
                                                          ),
                                                          onPressed:()
                                                          {
                                                            Navigator.of(context).pop();
                                                          },
                                                        )
                                                      ], // Action
                                                      backgroundColor: color_dark,
                                                    );
                                                  }
                                                );
                                              },
                                            ),
                                            alignment: Alignment.centerRight, 
                                          ),
                                          flex: 3,
                                        ), 
                                      ],
                                    ), 
                                    flex: 2
                                  ),
                                ],
                              ),
                              
                              decoration: BoxDecoration
                              (
                                borderRadius: BorderRadius.all(Radius.circular(15)),
                                color: color_light,
                                border: Border.all(width: 2, color: color_orange,),
                              ),

                              width: double.infinity,
                              margin: const EdgeInsets.all(15),
                              height: 150,
                            ),


                          /*
===================================PI===================================
*/
                          

         
                          ],
                        ),
                      ),
                      padding:
                          EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
                      color: Colors.black,
                      width: double.infinity,
                      //altura completa
                      //height: 800,

                      margin: EdgeInsets.only(top: 10),
                    ),
                  ),
                  margin: EdgeInsets.only(top: 10),
                ),
                flex: 14),
          ],
        ),
        height: MediaQuery.of(context).size.height,
        //padding: EdgeInsets.only(top: 40),
        //margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        //padding: 
        color: color_black,
      ),
    );
  }
}
