import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

import './others/globalVariables.dart';
import 'dashboard.dart';
import 'package:url_launcher/url_launcher.dart';



class Login extends StatefulWidget
{
  @override
  _LoginState createState() => _LoginState();
}



class _LoginState extends State<Login>
{
  TextEditingController _nome = TextEditingController();
  TextEditingController _password = TextEditingController();

  
  Future checkLoggedIn() async
  {
    if(isLogged == 1)
    {
      Navigator.pushReplacement(context,MaterialPageRoute
      (
        builder: (context)=>Dashboard(),
      ));
    }
    print("cccccccccccc");
  }

  Future pegarSP() async
  {
    SharedPreferences prefs  = await SharedPreferences.getInstance();
   
      userNome = (prefs.getString('nome') ?? "");
      userSenha = (prefs.getString('password') ?? "");
      isLogged = (prefs.getInt('isLogged') ?? 0);
      print("bbbbbbbbbb");
      print(userNome);
  }

  Future createUser() async
  {
    SharedPreferences prefs  = await SharedPreferences.getInstance();
    prefs.setString('nome', _nome.text);
    prefs.setString('password', _password.text);
    prefs.setInt('isLogged', 1);
    print("aaaaaaaaaaaa");
  }

  @override
  void initState()
  {
    super.initState();
    pegarSP().then((value) => checkLoggedIn());
  }


  
  @override
  Widget build(BuildContext context)
  {
    return Scaffold
    (
      body:Container 
      (
        child:Column
        (
          children:<Widget>
          [
            Expanded
            (
              child: Center
              (
                child: Center
                (
                  child: Center
                  (
                    child: Container
                    (
                      child: Column
                      (
                        children: <Widget>
                        [
                          Expanded
                          (
                            child: Container
                            (
                              child:Row
                              (
                                children: <Widget>
                                [
                                  Expanded
                                  (
                                    child: Container 
                                    (
                                      child: RichText
                                      (
                                        text: TextSpan
                                        (
                                          children: <TextSpan>
                                          [
                                            TextSpan
                                            (
                                              text: "HEA",
                                              style: GoogleFonts.roboto
                                              (
                                                fontSize: 50,
                                                color: color_light,
                                                fontWeight: FontWeight.w900,
                                                fontStyle: FontStyle.italic,
                                              ),
                                            ),
                                            
                                            TextSpan
                                            (
                                              text: "PPY",
                                              style: GoogleFonts.roboto
                                              (
                                                fontSize: 50,
                                                color: color_orange,
                                                fontWeight: FontWeight.w900,
                                                fontStyle: FontStyle.italic,
                                              ),
                                              
                                            ),
                                          ],
                                        ),
                                        textAlign: TextAlign.left,
                                      ),
                                      height: 60,
                                    ),
                                    flex:6
                                  ),

                                  Expanded
                                  (
                                    child:Align
                                    (
                                      alignment: Alignment.center,
                                      child: Container
                                      (
                                        child: Container
                                        (
                                          child: IconButton
                                          (
                                            icon: Icon
                                            (
                                              Icons.group,
                                              color: color_light,
                                            ),
                                            onPressed: ()
                                            {
                                              //criar um AlertDialog pra mostrar o nome do usuário
                                              showDialog
                                              (
                                                context: context,
                                                builder: (BuildContext context)
                                                {
                                                  return AlertDialog
                                                  (
                                                    title: Text
                                                    (
                                                      "Grupo Stack Underflow ",
                                                      style: GoogleFonts.ptSans
                                                      (
                                                        fontSize: 20,
                                                        color: color_light,
                                                        fontWeight: FontWeight.w600,
                                                        //fontStyle: FontStyle.italic,
                                                      ),
                                                    ),
                                                    content: SingleChildScrollView
                                                    (
                                                      child: ListBody
                                                      (
                                                        children: <Widget>
                                                        [
                                                          Text
                                                          (
                                                            '- DANIEL BORBA GUIMARÃES DA COSTA', 
                                                            style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                          ),
                                                          Text
                                                          (
                                                            '- DAVID GABRIEL BEILFUSS JORGE', 
                                                            style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                          ),
                                                          Text
                                                          (
                                                            '- MURILLO SAMPAIO OLIVEIRA ANTONIO', 
                                                            style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                          ),
                                                          Text
                                                          (
                                                            '- OTÁVIO AUGUSTO RIBEIRO ARAÚJO', 
                                                            style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                          ),
                                                          Text
                                                          (
                                                            '- SÔNIA RESENDE DE ARAÚJO', 
                                                            style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                          ),
                                                          Text
                                                          (
                                                            '- THAÍS RESENDE ARAÚJO BORGES BONFIM', 
                                                            style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                          ),

                                                          Text(
                                                            '',
                                                            style: TextStyle(
                                                                fontFamily: 'ProductSans',
                                                                fontSize: 15),
                                                          ),
                                                          

                                                          GestureDetector
                                                          (
                                                            onTap: () => launch('https://gitlab.com/otavio123ribeiro/heappy'),
                                                            child: Card
                                                            (
                                                              child: Row
                                                              (
                                                                children: <Widget>
                                                                [
                                                                  Expanded
                                                                  (
                                                                    flex: 3, // 60%
                                                                    child: IconButton
                                                                    (
                                                                      icon: Image.asset("assets/icon/interface/gitlab.png"),
                                                                      onPressed: () {},
                                                                    ),
                                                                  ),
                                                                  
                                                                  Expanded
                                                                  (
                                                                    flex: 21, // 60%
                                                                    child: Text
                                                                    (
                                                                      "GitLab Link",
                                                                      style: GoogleFonts.roboto
                                                                      (
                                                                        fontSize: 14,
                                                                        color: color_black,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                              shape: RoundedRectangleBorder
                                                              (
                                                                borderRadius: BorderRadius.circular(30.0),
                                                              ),
                                                              color: color_light,
                                                            ),
                                                          ),
                                                          
                                                          
                                                          Text(
                                                            '',
                                                            style: TextStyle(
                                                                fontFamily: 'ProductSans',
                                                                fontSize: 15),
                                                          ),
                                                          Text
                                                          (
                                                            'A ideia principal do aplicativo é realizar cálculos através de valores informados pelo usuário. Alguns deles são: porcentagem de gordura corporal, índice de massa corporal, taxa metabólica basal, gasto calórico diário, peso ideal, potencial muscular máximo...  Até o momento está previsto ao menos 4 "telas", onde 1 será uma provável tela de login, 1 será para o usuário inserir/atualizar suas informações, 1 será para mostrar os resultados calculados. Outras "telas" também podem ser inseridas futuramente. Os valores necessários para realizar os cálculos serão por exemplo: altura, idade, peso, sexo... Com essas informações, será possível realizar cálculos através de funções.Se faz necessário ressaltar que a aplicação terá validação médica, sendo a palavra de um responsável como final para quaisquer fins. No entanto, serão utilizados fórmulas e métodos muito conhecidos como as fórmulas de Mifflin-St Jeor, a equação revisada de Harris-Benedict. Ainda não estão definidos quais serão as bibliotecas, ou pacotes externos serão utilizados, no entanto, provavelmente serão necessárias soluções para salvamento de informações de forma local, exibição de gráficos (se for decidido sua implementação),  uma forma simples de mostrar avisos (possivelmente através de "toast notifications"). Já está sendo analisada a possibilidade de uso de um banco de dados, mas nada definido, tendo em vista que o prazo para desenvolvimento é curto e outros fatores.', 
                                                            style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                          ),
                                                          Text(
                                                            '',
                                                            style: TextStyle(
                                                                fontFamily: 'ProductSans',
                                                                fontSize: 15),
                                                          ),
                                                          Text(
                                                            'É importante para nós que os usuários permaneçam saudáveis enquanto atingem suas metas de condicionamento físico e bem-estar. Seja responsável e use seu melhor julgamento e bom senso. Fornecemos nossos serviços apenas para fins informativos e não podemos ser responsabilizados em caso de lesão ou se caso apresentar um problema de saúde. Em particular, embora a maior parte do conteúdo publicado pelos outros usuários em nossa comunidade seja útil, ela é proveniente de estranhos na Internet e nunca deve prevalecer sobre o bom senso ou o aconselhamento médico real. Ao usar nossos Serviços, você concorda, representa e garante que recebeu o consentimento do seu médico para participar dos Programas ou de qualquer uma das atividades relacionadas disponibilizadas para você em conexão com os Serviços. Além disso, você concorda, declara e garante que consultou seu médico antes de fazer qualquer alteração alimentar com base nas informações disponíveis através dos Serviços. As condições e habilidades de todos são diferentes, e participar dos Programas e outras atividades promovidas por nossos Serviços é por sua conta e risco. Se você optar por participar desses Programas e outras atividades, você o faz por sua livre e espontânea vontade, concordando, consciente e voluntariamente, assumindo todos os riscos associados a tais atividades. Os programas e outras atividades promovidas pelos Serviços podem representar riscos mesmo para aqueles que estão atualmente com boa saúde.',
                                                            style: GoogleFonts.roboto
                                                            (
                                                              fontSize: 14,
                                                              color: color_light,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    actions: <Widget>
                                                    [
                                                      FlatButton
                                                      (
                                                        child: Text
                                                        (
                                                          "Fechar",
                                                          style: GoogleFonts.roboto
                                                          (
                                                            fontSize: 15,
                                                            color: color_orange,
                                                            //fontWeight: FontWeight.w800,
                                                            //fontStyle: FontStyle.italic,
                                                          ),
                                                        ),
                                                        onPressed: ()
                                                        {
                                                          Navigator.of(context).pop();
                                                        },
                                                      ),
                                                    ],
                                                    backgroundColor: color_dark,
                                                  );
                                                },
                                              );
                                            },
                                          ),

                                          decoration: BoxDecoration
                                          (
                                            color: color_dark,
                                            border: Border.all(color: color_orange, width: 1),
                                            //border: Border.all(color: color_orange, width: 5.0),
                                            borderRadius: BorderRadius.all(Radius.circular(35.0)),
                                            image: DecorationImage
                                            (
                                              image: AssetImage('assets/images/wavey-fingerprint.png'),
                                              fit: BoxFit.cover
                                            )
                                          ),
                                          margin: EdgeInsets.only(right: 20),
                                          height: 43,
                                          width: 43,
                                          alignment: Alignment.center,
                                        ),
                                      ),
                                    ),

                                    
                                    flex: 2
                                  ),
                                ],
                              ),
                              width: double.infinity,
                              margin: EdgeInsets.only(top: 20, left: 30),
                            ),
                            flex: 2
                          ),

                          Expanded
                          (
                            child: Container
                            (
                              child: Text
                              (
                                "Seu App Fitness",
                                style: GoogleFonts.ptSans(
                                  fontSize: 16,
                                  color: color_light,
                                  fontWeight: FontWeight.w500,
                                  //fontStyle: FontStyle.italic,
                                ),
                                textAlign: TextAlign.left,
                              ),
                              margin: EdgeInsets.only(left: 10),
                              //padding: EdgeInsets.only(bottom: 15),
                              width: double.infinity,
                            ),
                            flex: 1
                          ),

                          Expanded
                          (
                            child: Center
                            (
                              child: Container
                              (
                                child: Column
                                (
                                  children: <Widget>
                                  [
                                    Expanded
                                    (
                                      child: Container
                                      (
                                        child:  new TextField
                                        (
                                          decoration: new InputDecoration
                                          (                          
                                            focusedBorder: OutlineInputBorder
                                            (
                                              borderRadius: BorderRadius.all(Radius.circular(30)),
                                              borderSide: BorderSide(width: 2, color: Colors.white),
                                            ),
                                            
                                            enabledBorder: OutlineInputBorder
                                            (
                                              borderRadius: BorderRadius.all(Radius.circular(30)),
                                              borderSide: BorderSide(width: 2,color: Colors.white),
                                            ),
                                            
                                            errorBorder: OutlineInputBorder
                                            (
                                              borderRadius: BorderRadius.all(Radius.circular(30)),
                                              borderSide: BorderSide(width: 1,color: Colors.black)
                                            ),
                                            hintText: 'Manuel Gustavo',
                                            hintStyle: TextStyle(fontSize: 16.0, color: Color.fromRGBO(255, 255, 255, 0.5)),
                        
                                            //helperText: '',
                                            labelText: 'Nome',
                                            labelStyle: TextStyle(fontSize: 16.0, color: color_orange),
                                            prefixIcon: const Icon
                                            (
                                              Icons.person,
                                              color: Colors.white,
                                            ),
                                            prefixText: ' ',
                                            suffixText: ' ',
                                            suffixStyle: const TextStyle(color: Colors.white),
                                            
                                          ),
                                          controller: _nome,
                                          //keyboardType: TextInputType.text,
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        margin: EdgeInsets.only(right: 20, left: 20, top: 5, bottom: 5),
                                        //padding: EdgeInsets.only( top: 7, bottom: 7),
                                        width: double.infinity,
                                      ),
                                    ),

                                    Expanded
                                    (
                                      child: Container
                                      (
                                        child:  new TextField
                                        (
                                          decoration: new InputDecoration
                                          (                          
                                            focusedBorder: OutlineInputBorder
                                            (
                                              borderRadius: BorderRadius.all(Radius.circular(30)),
                                              borderSide: BorderSide(width: 2, color: Colors.white),
                                            ),
                                            
                                            enabledBorder: OutlineInputBorder
                                            (
                                              borderRadius: BorderRadius.all(Radius.circular(30)),
                                              borderSide: BorderSide(width: 2,color: Colors.white),
                                            ),
                                            
                                            errorBorder: OutlineInputBorder
                                            (
                                              borderRadius: BorderRadius.all(Radius.circular(30)),
                                              borderSide: BorderSide(width: 1,color: Colors.black)
                                            ),
                                            hintText: '',
                                            hintStyle: TextStyle(fontSize: 16.0, color: Color.fromRGBO(255, 255, 255, 0.5)),
                        
                                            //helperText: '',
                                            labelText: 'Senha',
                                            labelStyle: TextStyle(fontSize: 16.0, color: color_orange),
                                            prefixIcon: const Icon
                                            (
                                              Icons.password_sharp,
                                              color: Colors.white,
                                            ),
                                            prefixText: ' ',
                                            suffixText: ' ',
                                            suffixStyle: const TextStyle(color: Colors.white),                                          
                                          ),
                                          controller: _password,
                                          //keyboardType: TextInputType.text,
                                          style: TextStyle(color: Colors.white),
                                          //esconder valor digitado
                                          obscureText: true,
                                        ),
                                        margin: EdgeInsets.only(right: 20, left: 20, top: 5, bottom: 5),
                                        width: double.infinity,
                                      ),
                                    ),
                                  ]
                                )
                              )
                            ),
                            flex: 3
                          ),



                          Container
                            (
                              child: TextButton
                              (
                                child: Text
                                (
                                  "Entrar",
                                  style: GoogleFonts.ptSans(
                                    fontSize: 16,
                                    color: color_dark,
                                    fontWeight: FontWeight.w800,
                                    //fontStyle: FontStyle.italic,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                                onPressed: ()
                                {
                                  createUser().then((value) => pegarSP().then((value) => checkLoggedIn()));
                                },
                                style: TextButton.styleFrom
                                (
                                  primary: Colors.pink,
                                  backgroundColor: color_orange,
                                  elevation: 10,
                                  shape: RoundedRectangleBorder
                                  (
                                    borderRadius: BorderRadius.circular(25.0),
                                  ),
                                ),
                              ),
                              width: double.infinity,
                              height: 35,
                              margin: EdgeInsets.only(top: 12, left: 60, right: 60, bottom: 12),
                            ),
                            
                        ],
                      ),

                      //color: Colors.amber[600],
                      constraints: const BoxConstraints
                      (
                        minWidth: 300,
                        maxWidth: 325,
                        minHeight: 350,
                        maxHeight: 360
                      ),

                      //adicionar bordar no container
                      decoration: BoxDecoration
                      (
                        color: color_dark,
                        border: Border.all(color: color_light, width: 2),
                        //border: Border.all(color: color_orange, width: 5.0),
                        borderRadius: BorderRadius.all(Radius.circular(45.0)),
                      ),

                      margin: const EdgeInsets.only(top: 25.0, bottom: 5.0),
                    ),
                  ),
                ),
              ),
              flex: 10,
            ),

            Expanded
            (
              child: Container
              (
                padding: const EdgeInsets.only(bottom: 10),
                //margin: const EdgeInsets.only(right: 200),
                child: Image.asset('assets/images/logo41.png'),
                constraints: BoxConstraints
                (
                  minWidth: 100,
                  maxWidth: 180,
                  minHeight: 200,
                  maxHeight: 201
                ),
              ),
              flex: 1,
            )
          ]
        ),
        //color: color_black,
        decoration: BoxDecoration
        (
          color: color_dark,
          image: DecorationImage
          (
            image: AssetImage('assets/images/wave-_1_.png'),
            fit: BoxFit.cover
          )
        ),
      )
      
    );
  }
}