import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:google_fonts/google_fonts.dart';

import 'package:heappy/dashboard.dart';
import 'package:heappy/screens/bootanimation.dart';
import './welcome.dart';
import './screens/home.dart';


void main() async
{
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations
  (
    [DeviceOrientation.portraitUp]
  ).then((value) => runApp(MyApp()));
}

class MyApp extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return MaterialApp(
      title: 'Heappy',
      theme: ThemeData
      (
        primarySwatch: Colors.blue,
        fontFamily: "Nunito",
      ),
      home: BootAnimation(),
      
    );
  }
}

class MyHomePage extends StatefulWidget
{
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
{
  @override
  Widget build(BuildContext context)
  {
    return Scaffold
    (
      body: Container
      (
        color: Colors.transparent,
      ),
    );
  }
}
