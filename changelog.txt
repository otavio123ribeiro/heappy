# Changelog
Todas as mudanças notáveis ​​neste projeto serão documentadas neste arquivo.

## Mudanças futuras (prioridades)

##  [0.9] - 16/12/2021 - Daniel Borba Guimarães da Costa
-   Versão final do projeto.

##  [0.8] - 16/12/2021 - Otávio Augusto
-   Foi adicionado o salvamento e busca do restantes das variáveis no através do SharedPreferences;
-   Foi corrigido pequenos erros gerais no código como um todo.

##  [0.7] - 14/12/2021 - Otávio Augusto
-   Foi corrigido o label do input Altura;
-   Foi criado toda uma lógica para validação dos intervalos aceitos e preenchimento para cada um dos inputs, afim de obter uma fidelidade nos resultados gerados pelas fórmulas matemáticas.
-   Foi redesenhado os boxes da tela Home, para manter uma estética mais agradavél.
-   Foi corrigido alguns bugs relacionados ao fluxo de ações do trigger de 'salvar dados' e carregamento do estado atual dos dados na interface;
-   Adicionado um botão de Link do repositório do projeto;

##  [0.5.4] - 14/12/2021 - David Gabriel
-   Foi criado os box na página de home;
-   Inserção dos Textfield para mostrar ao usuário a resposta dos cáculos pegando as variáveis globais;
-   Formatar a área home para que fique de acordo com o estilo do App;


##  [0.5.3] - 12/12/2021 - Otávio Augusto
-   Mudança no estilo de alguns alearts;
-   Adicionado uma Toast Message quando o botão de 'Salvar' for clicado;


##  [0.5.2] - 12/12/2021 - Daniel Borba Guimarães da Costa;
-   Correção de bug ao salvar valores inseridos pelo usuário no SharedPreferences;
-   Buscando valores do SharedPreferences na tela Perfil para o cálculo das fórmulas;


##  [0.5.1] - 10/12/2021 - Daniel Borba Guimarães da Costa
-   Correção de bug em relação ao radio button para sexo;
-   Adição de botões para informações sobre os níveis de atividade e objetivo;


##  [0.5] - 10/12/2021 - Daniel Borba Guimarães da Costa
-   Declaração de todas as variáveis globais utilizadas no sistema (aquivo globalVariables);
-   Declaração das fórmulas utilizadas para cálculo (aquivo globalFormulas);

##  [0.4.3] - 10/12/2021 - Otávio Augusto Ribeiro Araújo 
-   Adicionado um texto dinâmico de boas vindas na tela Perfil (altera de acordo com o peíodo e o nome do usuário);
-   Adicionado variáveis para gerenciamento do período do dia;
-   Corrigido um pequeno bug na tela de Welcome;
-   Outras pequenas mudanças gerais no código;


##  [0.4.1] - 10/12/2021 - Murillo Sampaio
-   Correção de bug: Texto digitado na interface de perfil continha cor errada;
-   Permanência de dados inseridos pelo usuário no arquivo SharedPreferences;
-   Melhoras na formatação de algumas partes do código;


##  [0.4] - 10/12/2021 - Murillo Sampaio
-   Mudanças nos visuais dos campos de texto anteriores;
-   Adição de caixas de texto para Idade, Abdomen, Pescoço, e uma caixa dinâmica para Quadril (apenas para mulheres);
-   Adição de dropboxes para Nivel de Atividade e Objetivo;
-   Mudança na posição da caixa de seleção de sexo;
-   Controladores de texto adicionados para todos os campos de texto;


##  [0.3.3] - 09/12/2021 - Murillo Sampaio
-   Mudanças no visual da página de perfil
-   Mudanças nas caixas de texto para Peso e altura
-   Mudanças nos radio boxes para sexo


##  [0.3.2] - 07/12/2021 - David Gabriel
-   Mudanças da página de perfil
-   Foi separado em cada container os seguintes itens: "peso","altura" e "sexo"
-   Foi adicionado caixas de texto no "peso" e na "altura"
-   Foi adicionado um radio no "sexo" para escolher entre "feminino" e "masculino"
-   No final foi implementado um botão para confirmar os dados inseridos


##  [0.3.1] - 17/11/2021 - Otávio Augusto
-   Adcionado um ícone do App;
-   Atualizado a versão atual do App;


##  [0.3] - 17/11/2021 - Otávio Augusto
-   Mudanças em alguns elementos da tela de Login, para melhor UX;
-   Mudança no tempo de apresentação da tela de Boot Animation;
-   Mudança na paleta de cores da interface;
-   Criadas as interfaces bases das telas de Home e Profile, para acomodar os componentes futuros;
-   Refinamento geral na interface para um padrão específico;
-   Adicionado um AlertDialog com as informações do projeto;
-   Ajustes gerais no código;
-   Criado um container para evitar interferência da barra de notificações no resto da interface;


##  [0.1.5] - 14/11/2021 - Otávio Augusto
-   Corrigido o erro de abertura da tela Dashboard;
-   Adicionado uma tela de 'Boot Animation';
-   Modificado a interface da tela de Login adicionando alguns outros componentes (RichText, TextSpan);


##  [0.1] - 13/11/2021 - Otávio Augusto
-   Foram realizados ajustes gerais no projeto inicial gerado pelo Framework, para se adequar ao necessário;
-	Foram adicionadas algumas telas (Home, Login, Dashboard, Profile);
-	Foi adicionado um arquivo de variáveis globais;
-	Foram adicionados alguns assets (fontes, imagens);
-	Foi adicionados alguns pacotes (google_fonts, shared_preferences);
-	Foi adicionado algumas funções para salvar, carregar, fazer a verificação de informações e consequentemente abrindo uma nova janela em definitivo;
-	Foram adicionados diversos componentes de interface gráfica, (textos, barra de navegação, containers, colunas, linhas, imagens, botões, input texto…);


##  [0.1] - 12/11/2021 - Otávio Augusto
-   Início do projeto;