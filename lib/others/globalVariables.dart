import 'package:flutter/material.dart';

final color_black = Color.fromRGBO(0, 0, 0, 1);
final color_dark = Color.fromRGBO(39, 39, 39, 1);
final color_light = Color.fromRGBO(233, 233, 233, 1);
final color_white = Color.fromRGBO(255, 255, 255, 1);
final color_orange = Color.fromRGBO(222, 255, 0, 1);

String userNome = "";
String userSenha = "";

double userPeso = 0.0;

int isLogged = 0;

int inputsErrorCheck = 0;



//----------------------- Perfil Info ----------------------
double massa_perfil_g = 0.0;
double altura_perfil_g = 0.0;
double idade_perfil_g = 0.0;
double abdomem_perfil_g = 0.0;
double pescoco_perfil_g = 0.0;
double quadril_perfil_g = 0.0;
int sexo_perfil_g = 0; // M = 1 / F = 2
String dropButton_atividade_perfil_g = 'Sedentário';
String dropButton_objetivo_perfil_g = 'Perder Peso';

//----------------------- Variáveis para fórmulas ----------------------
String indice = '';
double imc_perfil_g = 0;
double tmb_perfil_g = 0;
double lbm_perfil_g = 0;
double gc_perfil_g = 0;
double tdee_perfil_g = 0;
String tdee_perfil_mensage_g = 'Não informado ainda';
double pmm_perfil_g = 0;
double pmm_perfil_10_g = 0;
double pmm_perfil_15_g = 0;

double min_pi_perfil_g = 0;
double max_pi_perfil_g = 0;


  

//---- AUX ----


DateTime now = DateTime.now();
String horaEmString = "";
double horaEmDoble = 0;
String horaMensagem = "";